package com.devmhd.binaryblue.jra;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Me {
	
	private String name;
	private String status;
	private String about;
	private String location;
	private String avatar;
	private String age;
	
	public String getAge() {
		return age;
	}

	private ArrayList<ProfileAttribute> attributes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public ArrayList<ProfileAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(ArrayList<ProfileAttribute> attributes) {
		this.attributes = attributes;
	}

	public Me(JSONObject json) {
		
		attributes = new ArrayList<ProfileAttribute>();
		
		try {
			
			name = json.getJSONObject("data").getString("uname");
			status = json.getJSONObject("data").getString("status");
			about = json.getJSONObject("data").getString("about");
			location = json.getJSONObject("data").getString("location");
			avatar = json.getJSONObject("data").getString("avatar");
			age = json.getJSONObject("data").getString("age");
			
			JSONArray optionsArray = json.getJSONObject("data").getJSONArray("bits");
			
			for(int j=0; j<optionsArray.length(); ++j){
				//pInfo.addOption();
				
				int bit = Integer.parseInt(optionsArray.getString(j));
				
				int index = log2(bit);
				
				String name = G.staticProfileInfos.get(j).getSlug();
				
				String value = G.staticProfileInfos.get(j).getOptions().get(index);
				
				attributes.add(new ProfileAttribute(name, value, G.staticProfileInfos.get(j).getGrp()));
				
				Log.d(""+G.staticProfileInfos.get(j).getGrp()+name, value);
			}
			
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	static int log2(int x)
	{
	    return (int) (Math.log(x) / Math.log(2));
	}


}
