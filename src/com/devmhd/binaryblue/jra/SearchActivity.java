package com.devmhd.binaryblue.jra;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

import de.keyboardsurfer.android.widget.crouton.Crouton;









public class SearchActivity extends SherlockFragmentActivity implements ActionBar.TabListener{

	
	AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    ViewPager mViewPager;
    
  
	private String[] CONTENT = {
			"Simple",
			"Advanced"
	};
	
	

	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		

		 mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());
	        
	        
	       //ActionBar gets initiated
	        final ActionBar actionbar = getSupportActionBar();

	        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	        
	        
	        setTitle("Search JRA");
	        
	        mViewPager = (ViewPager) findViewById(R.id.pager);
	        mViewPager.setAdapter(mAppSectionsPagerAdapter);
	        
	        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
	            @Override
	            public void onPageSelected(int position) {
	                actionbar.setSelectedNavigationItem(position);
	            }
	        });
	        
	        for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
	            actionbar.addTab(
	                    actionbar.newTab()
	                            .setText(mAppSectionsPagerAdapter.getPageTitle(i))
	                            .setTabListener(this));
	        }

	       


	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		
//		getSupportMenuInflater().inflate(R.menu.activity_profilet, menu);
//		return true;
//	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	

	private class AppSectionsPagerAdapter extends FragmentPagerAdapter {

		public AppSectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {


			Fragment f = null;

			switch (i) {
			case 0:

				f = new SimpleSearchFragment();
				break;

			case 1:
				f = new AdvancedSearchFragment();
				break;


			}

			return f;

		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return CONTENT[position];
		}
	}


	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
	
//	@Override
//	public boolean onPrepareOptionsMenu(Menu menu) {
//	    MenuItem switchButton = menu.findItem(R.id.menu_fav);     
//	    if(isFav){
//	        switchButton.setIcon(R.drawable.ic_action_favourite);
//	        switchButton.setTitle("Remove from favourites");
//	    }else{
//	        switchButton.setIcon(R.drawable.ic_action_favourite_holo);
//	        switchButton.setTitle("Add to favourites");
//	    }
//	    return super.onPrepareOptionsMenu(menu);
//
//	}
	
	
	@Override
	protected void onDestroy() {
		Crouton.cancelAllCroutons();
		super.onDestroy();
	}


}
