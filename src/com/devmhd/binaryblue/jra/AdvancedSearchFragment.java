package com.devmhd.binaryblue.jra;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.holoeverywhere.ArrayAdapter;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.Spinner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;

public class AdvancedSearchFragment extends SherlockFragment {

	ProgressDialog pd;

	Spinner spSexuality, spGender, spMarital;

	String[] sexuality = {
			"All Sexuality",
			"Straight",
			"Gay",
			"Lesbian",
			"Bisexual"
			
	};

	String[] gender = {
			"All Gender",
			"Male",
			"Female",
			"Male and Female"
	};

	String[] marital = {
			"All Marital Status",
			"Single",
			"Married",
			"Seperated",
			"Divorced",
			"Widowed"
	};




	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_search_advanced, container, false);  

	}

	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	


		spSexuality = (Spinner) getView().findViewById(R.id.spSexuality);
		spGender = (Spinner) getView().findViewById(R.id.spGender);
		spMarital = (Spinner) getView().findViewById(R.id.spMarital);

		ArrayAdapter<String> adSex = new ArrayAdapter<String>(getActivity(), org.holoeverywhere.R.layout.simple_spinner_item, sexuality);
		adSex.setDropDownViewResource(org.holoeverywhere.R.layout.simple_spinner_dropdown_item);
		spSexuality.setAdapter(adSex);

		ArrayAdapter<String> adGender = new ArrayAdapter<String>(getActivity(), org.holoeverywhere.R.layout.simple_spinner_item, gender);
		adGender.setDropDownViewResource(org.holoeverywhere.R.layout.simple_spinner_dropdown_item);
		spGender.setAdapter(adGender);


		ArrayAdapter<String> adMarital = new ArrayAdapter<String>(getActivity(), org.holoeverywhere.R.layout.simple_spinner_item, marital);
		adMarital.setDropDownViewResource(org.holoeverywhere.R.layout.simple_spinner_dropdown_item);
		spMarital.setAdapter(adMarital);





		((Button) getView().findViewById(R.id.btnSearch)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

//				new AdvancedSearchTask().execute();
				
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					new AdvancedSearchTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				} else {
					new AdvancedSearchTask().execute();
				}

			}
		});


	}


	private JSONArray getPostParams(){

		int NO_OF_PARAMS = 3;

		int[] paramIndex = new int[NO_OF_PARAMS];


		paramIndex[0] = spSexuality.getSelectedItemPosition()-1;
		paramIndex[1] = spGender.getSelectedItemPosition()-1;
		paramIndex[2] = spMarital.getSelectedItemPosition()-1;


		int[] paramVals = new int[NO_OF_PARAMS];

		JSONArray jArray = new JSONArray();


		for(int i=0; i<NO_OF_PARAMS; ++i){

			if(paramIndex[i]==-1){

				if(i==0) paramVals[i] = 15;
				if(i==1) paramVals[i] = 7;
				if(i==2) paramVals[i] = 31;

			}else{

				paramVals[i] = (int) Math.pow(2.0, paramIndex[i]);
			}


			
			JSONObject jOb = new JSONObject();
			
			try {
				jOb.put("a", i);
				jOb.put("b", paramVals[i]);
			} catch (JSONException e) {
				
				e.printStackTrace();
			}
			
			
			
			jArray.put(jOb);
					
					


		}

		Log.d("sexu", ""+paramVals[0]);
		Log.d("gend", ""+paramVals[1]);
		Log.d("mari", ""+paramVals[2]);

		Log.d("JARRAY", jArray.toString());
		
		return jArray;

	}
	
	
	
	private ArrayList<NameValuePair> getPostParamsInPairs(){

		int NO_OF_PARAMS = 3;

		int[] paramIndex = new int[NO_OF_PARAMS];


		paramIndex[1] = spSexuality.getSelectedItemPosition()-1;
		paramIndex[0] = spGender.getSelectedItemPosition()-1;
		paramIndex[2] = spMarital.getSelectedItemPosition()-1;


		int[] paramVals = new int[NO_OF_PARAMS];

		
		ArrayList<NameValuePair> nvp = new ArrayList<NameValuePair>();

		for(int i=0; i<NO_OF_PARAMS; ++i){

			if(paramIndex[i]==-1){

				if(i==0) paramVals[i] = 15;
				if(i==1) paramVals[i] = 7;
				if(i==2) paramVals[i] = 31;

			}else{

				paramVals[i] = (int) Math.pow(2.0, paramIndex[i]);
			}


			
			nvp.add(new BasicNameValuePair(
					"search[" + i + "][a]", 
					""+i
					
					));
			
			nvp.add(new BasicNameValuePair(
					"search[" + i + "][b]", 
					""+paramVals[i]
					
					));
					
			
			
			Log.d("search[" + i + "][a]", 
					""+i);
			Log.d("search[" + i + "][b]", 
					""+paramVals[i]);
			


		}

		Log.d("sexu", ""+paramVals[0]);
		Log.d("gend", ""+paramVals[1]);
		Log.d("mari", ""+paramVals[2]);

		
		return nvp;
		

	}
	
	
	
	private class AdvancedSearchTask extends AsyncTask<Void, Void, JSONObject>{


		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(getActivity(), "", "Searching...");
			super.onPreExecute();
		}

		@Override
		protected JSONObject doInBackground(Void... params) {

			JSONObject searchResponse = null;

			try {

				JSONObject postParams = new JSONObject();
				postParams.put("search", getPostParams());
				
				Log.d("SEARCH BY", postParams.toString());
				
				searchResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_SUBMIT_SIMPLE_SEARCH, getPostParamsInPairs(), true);




			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Log.d("SEARCH_RESPONSE", searchResponse.toString());
			return searchResponse;


		}

		protected void onPostExecute(JSONObject searchResponse) {

			
			try {
				if(searchResponse.getBoolean("success")){
					
					JSONArray resultArray = searchResponse.getJSONObject("data").getJSONArray("res");
					
					G.currentSearchResults = new ArrayList<SearchResult>();
					for(int i=0; i<resultArray.length(); ++i){
						JSONObject result =  resultArray.getJSONObject(i);
						
						G.currentSearchResults.add(new SearchResult(
								Integer.parseInt(result.getString("id")), 
								result.getString("about"), 
								result.getString("avatar"), 
								result.getString("uname"),
								Integer.parseInt(result.getString("app_match"))
								
								));
						
					}
					
				}
				
				else G.currentSearchResults = null;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			pd.dismiss();
			
			startActivity(new Intent(getActivity(), SearchResultActivity.class));

		};

	}



}