package com.devmhd.binaryblue.jra;

import android.net.Uri;

public class Contact {

	

	public Contact(String _id, String displayName, Uri photoUri, String email, boolean selected) {
		super();
		this._id = _id;
		this.displayName = displayName;
		this.photoUri = photoUri;
		this.email = email.split(";")[0];
		this.selected = selected;
	}
	
	
	public String _id;
	public String displayName;

	public Uri photoUri;
	public String email;
	public boolean selected;
	
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}


	
	

}
