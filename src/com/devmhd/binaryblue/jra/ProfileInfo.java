package com.devmhd.binaryblue.jra;

import java.util.ArrayList;

public class ProfileInfo {
	
	private String slug;
	private int grp;
	
	private ArrayList<String> options;

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public int getGrp() {
		return grp;
	}

	public void setGrp(int grp) {
		this.grp = grp;
	}

	public ArrayList<String> getOptions() {
		return options;
	}

	public void setOptions(ArrayList<String> options) {
		this.options = options;
	}

	public ProfileInfo(String slug, int grp) {
		super();
		this.slug = slug;
		this.grp = grp;
		this.options = new ArrayList<String>();
	}
	
	public void addOption(String op)
	{
		options.add(op);
	}
	
	
	
	

}
