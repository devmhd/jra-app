package com.devmhd.binaryblue.jra;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import android.content.Context;

public class FileStorage {

	public static void writeSearchResultsToFile(Context context) throws IOException{

		FileOutputStream fos = context.openFileOutput("savedsearchresults", Context.MODE_PRIVATE);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(G.allResults);
		os.close();
	} 


	@SuppressWarnings("unchecked")
	public static void loadSearchResultsFromFile(Context context) throws IOException, ClassNotFoundException{

		FileInputStream fis = context.openFileInput("savedsearchresults");
		ObjectInputStream is = new ObjectInputStream(fis);
		G.allResults = (ArrayList<SearchResult>) is.readObject();
		is.close();

	}


	public static void writeFavouritesToFile(Context context) throws IOException{

		FileOutputStream fos = context.openFileOutput("savedfavourites", Context.MODE_PRIVATE);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(G.favourites);
		os.close();
	} 


	@SuppressWarnings("unchecked")
	public static void loadFavouritesFromFile(Context context) throws IOException, ClassNotFoundException{

		FileInputStream fis = context.openFileInput("savedfavourites");
		ObjectInputStream is = new ObjectInputStream(fis);
		G.favourites = (ArrayList<SearchResult>) is.readObject();
		is.close();

	}
	
	
	public static void writeFriendsToFile(Context context) throws IOException{

		FileOutputStream fos = context.openFileOutput("savedfriends", Context.MODE_PRIVATE);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(G.contacts);
		os.close();
	} 


	@SuppressWarnings("unchecked")
	public static void loadFriendsFromFile(Context context) throws IOException, ClassNotFoundException{

		FileInputStream fis = context.openFileInput("savedfriends");
		ObjectInputStream is = new ObjectInputStream(fis);
		G.contacts = (ArrayList<SearchResult>) is.readObject();
		is.close();

	}
	
	public static void writeFriendRequestsToFile(Context context) throws IOException{

		FileOutputStream fos = context.openFileOutput("savedfriendrequests", Context.MODE_PRIVATE);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(G.frequests);
		os.close();
	} 


	@SuppressWarnings("unchecked")
	public static void loadFriendRequestsFromFile(Context context) throws IOException, ClassNotFoundException{

		FileInputStream fis = context.openFileInput("savedfriendrequests");
		ObjectInputStream is = new ObjectInputStream(fis);
		G.frequests = (ArrayList<FriendRequest>) is.readObject();
		is.close();

	}



}
