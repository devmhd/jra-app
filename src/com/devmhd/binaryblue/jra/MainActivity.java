package com.devmhd.binaryblue.jra;



import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.holoeverywhere.app.ProgressDialog;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBarDrawerToggle;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;





public class MainActivity extends SherlockFragmentActivity implements
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener{







	ProgressDialog pd;

	DrawerLayout drawerLayout;
	ListView drawerList;

	LocationClient locationClient;


	ActionBarDrawerToggle drawerToggle;


	JRALiveMapSearchTask jraLiveMapSearchTask;
	MapSearchTask mapSearchTask;
	MyLocationUpdateTask myLocationUpdateTask;


	@Override
	protected void onResume() {
		jraLiveMapSearchTask = new JRALiveMapSearchTask();
		mapSearchTask = new MapSearchTask();
		myLocationUpdateTask = new MyLocationUpdateTask();
		super.onResume();
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		locationClient = new LocationClient(this, this, this);




		setTitle("Juicy Red Apple");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerList = (ListView) findViewById(R.id.left_drawer);

		drawerList.setAdapter(new NavAdapter(this, G.navItems));

		drawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int selection,
					long arg3) {


				openFragment(selection);

				drawerLayout.closeDrawer(drawerList);




			}
		});



		drawerToggle = new ActionBarDrawerToggle(

				this, 
				getSupportActionBar(),
				drawerLayout, 
				R.drawable.ic_drawer, 
				R.string.drawerOpenString, 
				R.string.drawerCloseString
				);

		drawerToggle.syncState();
		drawerLayout.setDrawerListener(drawerToggle);

		//		 getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		drawerToggle.setDrawerIndicatorEnabled(true);


		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, new HomeFragment());
		transaction.commit();


	}

	private void openFragment(int selection) {




		if(selection == 0){

			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.content_frame, new HomeFragment());
			transaction.addToBackStack(null)
			.commit();


		}

		if(selection == 1){

			Intent inte = new Intent(MainActivity.this, MyProfileActivity.class);
			inte.putExtra("multiple", true);
			startActivity(inte);
		}

		if(selection == 2){

			getSupportFragmentManager()	.beginTransaction()
			.replace(R.id.content_frame, new InboxFragment()).addToBackStack(null)
			.commit();


		}


		if(selection == 3){

			getSupportFragmentManager()	.beginTransaction()
			.replace(R.id.content_frame, new FriendRequestsFragment()).addToBackStack(null)
			.commit();


		}


		if(selection == 4){

			getSupportFragmentManager()	.beginTransaction()
			.replace(R.id.content_frame, new FavouritesFragment()).addToBackStack(null)
			.commit();


		}

		if(selection == 5){

			getSupportFragmentManager()	.beginTransaction()
			.replace(R.id.content_frame, new FriendsFragment()).addToBackStack(null)
			.commit();


		}



		if(selection == 6){ //Fav Map


			mapSearchTask.execute(1);
		}

		if(selection == 7){ //Contact Map


			mapSearchTask.execute(0);

		}

		if(selection == 8){ //JRA LIVE

			jraLiveMapSearchTask.execute();


		}
		
		if(selection == 9){ //JRA LIVE

			startActivity(new Intent(this, ImportContactsActivity.class));


		}
		
		if(selection == 10){ //JRA LIVE

			startActivity(new Intent(this, SettingsActivity.class));


		}
		
		if(selection == 11){ //JRA LIVE

			logout();


		}


	}


	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//Used to put dark icons on light action bar


		getSupportMenuInflater().inflate(R.menu.activity_main, menu);




		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {
		case R.id.menu_search:
			startActivity(new Intent(this, SearchActivity.class));
			break;

		case R.id.menu_settings:
			startActivity(new Intent(this, SettingsActivity.class));
			break;

		case R.id.menu_logout:
			logout();
			break;


		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}





	@Override
	public void onBackPressed() {




		FragmentManager fm = getSupportFragmentManager();

		if(fm.getBackStackEntryCount()==0)
			super.onBackPressed();
		else 
			fm.popBackStack();


	}








	private boolean servicesConnected() {
		// Check that Google Play services is available
		int resultCode =
				GooglePlayServicesUtil.
				isGooglePlayServicesAvailable(this);
		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d(G.DEBUG_TAG, "Location Updates: "+
					"Google Play services is available.");
			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {

			Log.d(G.DEBUG_TAG, "Location Updates: "+
					"Google Play services is not available.");
			return false;


		}



	}




	@Override
	public void onConnectionFailed(ConnectionResult result) {

		Crouton.makeText(this, "Location Service Failed", Style.ALERT).show();

	}

	@Override
	public void onConnected(Bundle connectionHint) {

		if (locationClient != null) {

			//			Location myLocation = locationClient.getLastLocation();
			//			Double lat = myLocation.getLatitude();
			//			Double lng = myLocation.getLongitude();
			//						Toast.makeText(MainActivity.this, "LATLNG " + lat + " " + lng, Toast.LENGTH_LONG)
			//								.show();
			//						
			//
			//			
			//			G.initialPosition = new LatLng(lat, lng);
			//			
			//			myLocationUpdateTask.execute("" + lat, ""+lng);
			//			
			//			locationClient.disconnect();





			locationClient.requestLocationUpdates(new LocationRequest(), new LocationListener() {



				@Override
				public void onLocationChanged(Location myLocation) {
					Double lat = myLocation.getLatitude();
					Double lng = myLocation.getLongitude();
					//								Toast.makeText(MainActivity.this, "LATLNG " + lat + " " + lng, Toast.LENGTH_LONG)
					//										.show();



					G.initialPosition = new LatLng(lat, lng);

					myLocationUpdateTask.execute("" + lat, ""+lng);

					locationClient.disconnect();

				}
			});

			//					Location myLocation = locationClient.getLastLocation();


		}
		else
			Crouton.makeText(this, "Location Services Unavailable", Style.ALERT).show();

	}

	@Override
	public void onDisconnected() {


	}


	@Override
	protected void onStart() {
		super.onStart();

		if(servicesConnected())
		{	
			if (locationClient!=null) {
				locationClient.connect();

			}else{

				Crouton.makeText(this, "Please enable location services in Settings", Style.ALERT).show();
			}

		}
		else
			Crouton.makeText(this, "Location is not available because Google Play Services are not installed", Style.ALERT).show();


	}

	/*
	 * Called when the Activity is no longer visible.
	 */
	@Override
	protected void onStop() {
		// Disconnecting the client invalidates it.
		locationClient.disconnect();
		super.onStop();
	}


	@Override
	protected void onDestroy() {



		super.onDestroy();
	}


	@Override
	protected void onPause() {
		jraLiveMapSearchTask.cancel(true);
		myLocationUpdateTask.cancel(true);
		mapSearchTask.cancel(true);
		super.onPause();
	}





	private class MyLocationUpdateTask extends AsyncTask<String, Void, JSONObject>{


		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

		@Override
		protected JSONObject doInBackground(String... params) {

			JSONObject searchResponse = null;

			try {

				ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
				postVars.add(new BasicNameValuePair(URL.VAR_MY_LOCATION_LAT, (String)params[0]));
				postVars.add(new BasicNameValuePair(URL.VAR_MY_LOCATION_LNG, (String)params[1]));
				searchResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_SUBMIT_LOCATION_UPDATE, postVars, true);




			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(searchResponse!=null) Log.d(G.DEBUG_TAG, "MY_LOCATION_UPDATE_RESPONSE: "+ searchResponse.toString());
			return searchResponse;


		}

		protected void onPostExecute(JSONObject searchResponse) {



		};

	}








	private class JRALiveMapSearchTask extends AsyncTask<Void, Void, Integer>{


		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(MainActivity.this, "", "Finding location...");
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(Void... params) {

			JSONArray searchResponse = null;

			try {




				searchResponse = G.getJsonArray(URL.URL_BASE + URL.METHOD_GET_JRA_LIVE, null, false);

				G.currentMapResults = new ArrayList<MapResult>();
				for(int i=0; i<searchResponse.length(); ++i){

					JSONObject result =  searchResponse.getJSONObject(i);

					G.currentMapResults.add(new MapResult(
							Integer.parseInt(result.getString("id")),  
							result.getString("uname"), 
							result.getString("avatar"),  

							new LatLng(

									Double.parseDouble(result.getString("lat")), 
									Double.parseDouble(result.getString("lan"))

									)

							));



				}






			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Log.d(G.DEBUG_TAG, "MAP_SEARCH_RESPONSE: "+ searchResponse.toString());
			return 0;


		}

		protected void onPostExecute(Integer nothing) {

			pd.dismiss();

			Intent in = new Intent(MainActivity.this, MapActivity.class);
			in.putExtra("title", "JRA LIVE Map");
			in.putExtra("multiple", true);
			startActivity(in);


		};

	}





	private class MapSearchTask extends AsyncTask<Integer, Void, Integer>{


		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(MainActivity.this, "", "Finding location...");
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(Integer... params) {

			JSONObject searchResponse = null;

			try {

				if(params[0] == 0) searchResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_GET_CONTACTS, null, false);
				if(params[0] == 1) searchResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_GET_FAVOURITES, null, false);

				G.currentMapResults = new ArrayList<MapResult>();

				if(searchResponse.getBoolean("success")){

					JSONArray resultArray = searchResponse.getJSONObject("data").getJSONArray("dtls");

					for(int i=0; i<resultArray.length(); ++i){

						JSONObject singleResult = resultArray.getJSONObject(i);



						if (!(singleResult.getString("lat").equals("0"))) {


							G.currentMapResults.add(new MapResult(Integer
									.parseInt(singleResult.getString("id")),
									singleResult.getString("uname"),
									singleResult.getString("avatar"),
									new LatLng(

											Double.parseDouble(singleResult.getString("lat")), 
											Double.parseDouble(singleResult.getString("lng"))

											)));
						}

					}


				}






			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Log.d(G.DEBUG_TAG,"MAP_SEARCH_RESPONSE: "+ searchResponse.toString());
			return params[0];


		}

		protected void onPostExecute(Integer params) {




			pd.dismiss();

			Intent in = new Intent(MainActivity.this, MapActivity.class);
			in.putExtra("multiple", true);

			in.putExtra("title", ((params==0)?"My Contacts on Map":"My Favourites on Map"));

			startActivity(in);


		};

	}



	private void logout() {

		Storage.clearAll(getApplicationContext());
		G.httpclient = G.getThreadSafeClient();

		startActivity(new Intent(this, SplashActivity.class));
		finish();

	}




}