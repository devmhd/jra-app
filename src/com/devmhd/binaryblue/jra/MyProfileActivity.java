package com.devmhd.binaryblue.jra;

import java.io.UnsupportedEncodingException;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.AlertDialog.Builder;
import org.holoeverywhere.app.ProgressDialog;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.android.volley.toolbox.NetworkImageView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;




public class MyProfileActivity extends SherlockFragmentActivity implements ActionBar.TabListener{

	ProgressDialog progressDialog;
	AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    ViewPager mViewPager;
    
    LoadMyProfileTask loadMyProfileTask = new LoadMyProfileTask();
    
    NetworkImageView niv;
    
    private Builder addFriendDialogBuilder, sendMessageDialogBuilder;
	private AlertDialog addFriendDiag, sendMessageDiag;
	
	private String[] CONTENT = {
			"Profile",
			"Info",
			"LOOKING FOR"
	};
	
	
	boolean isFav = false;

	
	private void initDiag(){
		addFriendDialogBuilder = new AlertDialog.Builder(this);		
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		final View diagView = inflater.inflate(R.layout.dialog_friend_req, null);
		addFriendDialogBuilder.setView(diagView);
		addFriendDialogBuilder.setTitle("Add Sheldon Cooper");
		addFriendDialogBuilder.setPositiveButton("Send Request", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {

				Crouton.makeText(MyProfileActivity.this, "Contact Request Sent", Style.CONFIRM).show();

			}
		});

		addFriendDialogBuilder.setNegativeButton("Cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				addFriendDiag.cancel();

			}
		});

		addFriendDiag = addFriendDialogBuilder.create();
		
		//=============================
		
		sendMessageDialogBuilder = new AlertDialog.Builder(this);		
		
		final View diagMsgView = inflater.inflate(R.layout.dialog_send_message, null);
		sendMessageDialogBuilder.setView(diagMsgView);
		sendMessageDialogBuilder.setTitle("New Message");
		sendMessageDialogBuilder.setPositiveButton("Send", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {

				Crouton.makeText(MyProfileActivity.this, "Message Sent", Style.CONFIRM).show();

			}
		});

		sendMessageDialogBuilder.setNegativeButton("Cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				sendMessageDiag.cancel();

			}
		});

		sendMessageDiag = sendMessageDialogBuilder.create();

	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myprofile);
		// Show the Up button in the action bar.
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		

		 

	        initDiag();
	        niv = (NetworkImageView) findViewById(R.id.nivMyProfilePic);
	        loadMyProfileTask.execute();
	        
	        
	        
	        
	}
	
	
	void initPager(){
		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());
        
        
	       //ActionBar gets initiated
	        final ActionBar actionbar = getSupportActionBar();

	        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	        
	        
	 
	        
	        mViewPager = (ViewPager) findViewById(R.id.pager);
	        mViewPager.setAdapter(mAppSectionsPagerAdapter);
	        
	        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
	            @Override
	            public void onPageSelected(int position) {
	                actionbar.setSelectedNavigationItem(position);
	            }
	        });
	        
	        for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
	            actionbar.addTab(
	                    actionbar.newTab()
	                            .setText(mAppSectionsPagerAdapter.getPageTitle(i))
	                            .setTabListener(this));
	        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		getSupportMenuInflater().inflate(R.menu.activity_myprofilet, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			
			finish();
			return true;
			
//		case R.id.menu_fav:
//			
//			if(isFav){
//				isFav = false;
//				Crouton.makeText(this, "Removed from favourites", Style.INFO).show();
//			}
//			else{
//				isFav = true;
//				Crouton.makeText(this, "Added to favourites", Style.CONFIRM).show();
//			}
//			
//			invalidateOptionsMenu();
//			return true;
//		
//		case R.id.menu_add_friend:
//			addFriendDiag.show();
//			return true;
//			
//		case R.id.menu_send_message:
//			sendMessageDiag.show();
//			return true;
//			
//		
//			
		}
		return super.onOptionsItemSelected(item);
	}


	private class AppSectionsPagerAdapter extends FragmentPagerAdapter {

		public AppSectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {


			Fragment f = null;

			switch (i) {
			case 0:

				f = new MyProfileMainFragment();
				break;

			case 1:
				f = new MyProfileInfoFragment();
				break;

			case 2:
				f = new MyProfileLookinFragment();
				break;


			}

			return f;

		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return CONTENT[position];
		}
	}


	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
	
//	@Override
//	public boolean onPrepareOptionsMenu(Menu menu) {
//	    MenuItem switchButton = menu.findItem(R.id.menu_fav);     
//	    if(isFav){
//	        switchButton.setIcon(R.drawable.ic_action_favourite);
//	        switchButton.setTitle("Remove from favourites");
//	    }else{
//	        switchButton.setIcon(R.drawable.ic_action_favourite_holo);
//	        switchButton.setTitle("Add to favourites");
//	    }
//	    return super.onPrepareOptionsMenu(menu);
//
//	}
	
	
	
	
	
	
	@Override
	protected void onDestroy() {
		Crouton.cancelAllCroutons();
		
		loadMyProfileTask.cancel(true);
		super.onDestroy();
	}
	
	
	private class LoadMyProfileTask extends AsyncTask<Void,Void, JSONObject>{

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(MyProfileActivity.this, "", "Loading");
			super.onPreExecute();
		}
		
		@Override
		protected JSONObject doInBackground(Void... params) {
			
			JSONObject profileJson = null;
			
			try {
				profileJson = G.getJsonObject(URL.URL_BASE + URL.METHOD_GET_MY_PROFILE, null, false);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return profileJson;
		}
		@Override
		protected void onPostExecute(JSONObject result) {
			progressDialog.dismiss();
			Log.d("RESPONSE", result.toString());
			
			G.me = new Me(result);
			
			setTitle(G.me.getName());
			
			initPager();
			super.onPostExecute(result);
			
			
		}
		
	}
	
	


}
