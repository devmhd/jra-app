package com.devmhd.binaryblue.jra;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.actionbarsherlock.app.SherlockFragment;

public class MyProfileLookinFragment extends SherlockFragment {




	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_myprofile_lookin, container, false);  

	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	

		int padding10dp = G.dpToPx(10);


		LinearLayout llLookin = (LinearLayout) getView().findViewById(R.id.llLookin);

		LayoutParams rowParams = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		LayoutParams tvParams = new LayoutParams(
				0, LayoutParams.WRAP_CONTENT, 1f);


		for (ProfileAttribute pAtt : G.me.getAttributes()) {


			LinearLayout row = new LinearLayout(getActivity());
			row.setLayoutParams(rowParams);

			TextView tvLeft = new TextView(getActivity());
			tvLeft.setLayoutParams(tvParams);
			tvLeft.setTextColor(Color.parseColor("#33b5e5"));
			tvLeft.setText(pAtt.getName());
			tvLeft.setPadding(padding10dp, padding10dp, padding10dp, padding10dp);

			TextView tvRight = new TextView(getActivity());
			tvRight.setLayoutParams(tvParams);
			tvRight.setText(pAtt.getValue());
			tvRight.setPadding(padding10dp, padding10dp, padding10dp, padding10dp);


			row.setOrientation(LinearLayout.HORIZONTAL);
			row.addView(tvLeft);
			row.addView(tvRight);

			if(pAtt.getGroup()==2) llLookin.addView(row);



		}


	}



}