package com.devmhd.binaryblue.jra;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TOSActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTitle("JRA Terms and conditions");
		
		
		if(Storage.isAgreedToTos(getApplicationContext())){
			
			Intent in = new Intent(TOSActivity.this, SplashActivity.class);
			startActivity(in);
			
		}else{
			
			
			
			setContentView(R.layout.activity_tos);
			
			Button btnAgree = (Button) findViewById(R.id.btntosagree);
			Button btnDisAgree = (Button) findViewById(R.id.btntosdisagree);
			
			TextView tvTos = (TextView) findViewById(R.id.tvtos);
			
			String tosString = readFile("ToS.txt");
			 
			tvTos.setText(tosString);
			
			
			btnAgree.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					
					Storage.setAgreedToTos(getApplicationContext(), true);
					Intent in = new Intent(TOSActivity.this, SplashActivity.class);
					startActivity(in);
					
				}
			});
			
			btnDisAgree.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					finish();
					
				}
			});
			
		}
		
		
		
		
		
		
		
	}

	
	
	private String readFile(String fname)
	{
		AssetManager am = getAssets();
		InputStream is = null;
		try {
			is = am.open(fname);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		InputStreamReader inputStreamReader = new InputStreamReader(is);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		StringBuilder builder = new StringBuilder();
		String aux = "";
		try {
			while ((aux = bufferedReader.readLine()) != null) {
				builder.append(aux + '\n');
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return builder.toString();
	}

}
