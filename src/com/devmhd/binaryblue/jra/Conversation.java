package com.devmhd.binaryblue.jra;

public class Conversation {
	
	private String msgBody;
	private int senderId;
	private String senderName;
	private String senderAvatar;
	
	private int receiverId;
	private String receiverName;
	private String receiverAvatar;
	
	public int getReceiverId() {
		return receiverId;
	}
	public String getReceiverName() {
		return receiverName;
	}
	public String getReceiverAvatar() {
		return receiverAvatar;
	}
	public String getMsgBody() {
		return msgBody;
	}
	public int getSenderId() {
		return senderId;
	}
	public String getSenderName() {
		return senderName;
	}
	public String getSenderAvatar() {
		return senderAvatar;
	}
	
	public Conversation(String msgBody, int senderId, String senderName,
			String senderAvatar, int receiverId, String receiverName,
			String receiverAvatar) {
		super();
		this.msgBody = msgBody;
		this.senderId = senderId;
		this.senderName = senderName;
		this.senderAvatar = senderAvatar;
		this.receiverId = receiverId;
		this.receiverName = receiverName;
		this.receiverAvatar = receiverAvatar;
	}
	
	
	
	
	
	
	

}
