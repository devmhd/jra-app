package com.devmhd.binaryblue.jra;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import android.widget.TextView;



public class PersonGridAdapter extends ArrayAdapter<SearchResult> {

	Context context;
	
	ArrayList<SearchResult> persons;
	
	
	public PersonGridAdapter(Context context,
			ArrayList<SearchResult> persons) {
		super(context, R.layout.grid_cell_person, persons);
	
		
		this.context = context;
		
		this.persons = persons;
		
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		View v = convertView;
		if(v==null)
		{
			Log.d("NULL", "VIIIEWWWW NNUUULLLLLL");
			LayoutInflater li = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(R.layout.grid_cell_person, null);
		}
		
		TextView tvName = (TextView) v.findViewById(R.id.tvName);
		SquareImageView ivThumb = (SquareImageView) v.findViewById(R.id.ivThumb);
		ImageView ivRating = (ImageView) v.findViewById(R.id.ivRating);
		
		
		//Log.d("TAAG", "tvBig should show: "+listArray[position].desc+" tvSmall should:"+ listArray[position].amount);
		
		
		
		
		if (tvName!=null) tvName.setText(persons.get(position).getName());
		if(ivThumb!=null) ivThumb.setImageUrl(persons.get(position).getAvatar(), G.imageLoader);
		
		if(ivRating!=null) ivRating.setImageResource(G.getRatingImg(persons.get(position).getAppCount()));
		
		
		
//		if (tvSmall!=null) tvSmall.setText(""+listArray[position].getShortDesc());
//		
		//Date d = new Date(listArray[position].time);
		
		//if (tvTime!=null) tvTime.setText((new SimpleDateFormat("hh:mm aa MMM dd")).format(d));
		
		
		
		v.setTag(persons.get(position).getPersonId());
		
		
		
		return v;
	}
	
	

}
