package com.devmhd.binaryblue.jra;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.LinearLayout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.Window;


import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public class SplashActivity extends SherlockActivity {

	
	String okUname;
	String okPass;
	boolean isOkManual;
	
	
	AlertDialog.Builder diagNoNetwork;
	Animation logoFadeIn, logoTranslate, loginFadeIn, fromLeftTranslate, fromRightTranslate, fromBottomTranslate;
	
	ProgressDialog progressDialog;
	ImageView ivLogo;
	LinearLayout loginLayout, ll1, ll2;
	Button btn;
	EditText etUname, etPass;
	TextView tvSignUpLink;
	
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		startActivity(new Intent(this, MainActivity.class));
		
		hideTitle();
		setContentView(R.layout.activity_login);

		
		initNoNetDiag();
		
		
		
		ivLogo = (ImageView) findViewById(R.id.ivLogo);
		loginLayout = (LinearLayout) findViewById(R.id.loginLayout);
		ll1 = (LinearLayout) findViewById(R.id.ll1);
		ll2 = (LinearLayout) findViewById(R.id.ll2);
		
		
		btn = (Button) findViewById(R.id.btn);
		
		
		etUname = (EditText) findViewById(R.id.etuname);
		etPass = (EditText) findViewById(R.id.etpass);
		
		
		tvSignUpLink = (TextView) findViewById(R.id.tvSignUpLink);
		
		
		tvSignUpLink.setText(Html.fromHtml(
                "<a href=\"" + URL.URL_SIGNUP +"\">Sign Up</a> "));
		tvSignUpLink.setMovementMethod(LinkMovementMethod.getInstance());
		
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				
				final String uname = etUname.getText().toString();
				final String pass = etPass.getText().toString();
				
				okUname = etUname.getText().toString();
				okPass = etPass.getText().toString();
				
				
				// the snippet below hides the softkeyboard (if present) 
				if (SplashActivity.this.getCurrentFocus()!=null) {  
					InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(SplashActivity.this
							.getCurrentFocus().getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
				}
				
				
				
				new HugeAsyncTask().execute(uname, pass, true);
				
				

				
				
				
			}
		});
		
		
		if(savedInstanceState!=null) 
		{
			
			ll1.setVisibility(View.GONE);
			ll2.setVisibility(View.GONE);
			loginLayout.setVisibility(View.VISIBLE);
			
			return;
			
		}
		
		
		if(getIntent().getExtras()!=null)
		{
			if(getIntent().getExtras().getBoolean("newLogin"))
			{
				ll1.setVisibility(View.GONE);
				ll2.setVisibility(View.GONE);
				loginLayout.setVisibility(View.VISIBLE);
				
				return;
				
			}
		}
		
		
		logoFadeIn = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fade_in);
		loginFadeIn = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fade_in);
		logoTranslate = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fade_out);
		
		fromLeftTranslate = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.translate_from_left);
		fromRightTranslate = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.translate_from_right);

		fromBottomTranslate = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.translate_from_bottom);
		
		
		logoFadeIn.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				
						if(G.isNetworkConnected(getApplicationContext()))
						{
							if(Storage.isLoggedIn(getApplicationContext())){
								
								Log.d(G.DEBUG_TAG, "AUTO LOGIN WITH UNAME: " + Storage.getLoggerEmail(getApplicationContext()));
								Log.d(G.DEBUG_TAG, "AUTO LOGIN WITH PASS: " + Storage.getLoggerPassword(getApplicationContext()));
								
								
								new HugeAsyncTask().execute(
										Storage.getLoggerEmail(getApplicationContext()), 
										Storage.getLoggerPassword(getApplicationContext()),
										false
									);
								
							}
								
								
							else
								ivLogo.startAnimation(logoTranslate);
						}
						
						else
							diagNoNetwork.show();
						
	
				
				
			}
		});
		
		logoTranslate.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
			
				loginLayout.setVisibility(View.VISIBLE);
				ll1.setVisibility(View.GONE);
				ll2.setVisibility(View.GONE);
//				loginLayout.startAnimation(loginFadeIn);
				
				etUname.startAnimation(fromLeftTranslate);
				etPass.startAnimation(fromRightTranslate);
				tvSignUpLink.startAnimation(fromBottomTranslate);
				btn.startAnimation(fromBottomTranslate);
				
			}
		});
		
		
		ivLogo.startAnimation(logoFadeIn);
		
		
		
		G.init(getApplicationContext());

	}
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getSupportMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}
	
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	void hideTitle()
	{
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
        {
        	requestWindowFeature(Window.FEATURE_NO_TITLE);
           
        }
		
		else getActionBar().hide();
	}

	
	
	
	
	@Override
	protected void onDestroy() {
		Crouton.cancelAllCroutons();
		super.onDestroy();
	}
	
	
	private void initNoNetDiag(){
		diagNoNetwork = new AlertDialog.Builder(this);
		diagNoNetwork.setTitle("No Network");
		diagNoNetwork.setMessage("JRA needs an active internet connection to work properly");
		diagNoNetwork.setIcon(R.drawable.ic_alert);
		diagNoNetwork.setCancelable(false);
		diagNoNetwork.setPositiveButton("Retry", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				if(G.isNetworkConnected(getApplicationContext()))
				{
						
					ivLogo.startAnimation(logoTranslate);
//						startActivity(new Intent(SplashActivity.this, MainActivity.class));
//						finish();
//					
				}
				else
					diagNoNetwork.show();
				
			}
		});
		
		diagNoNetwork.setNeutralButton("Settings", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			
				startActivity(new Intent(Settings.ACTION_SETTINGS));
				diagNoNetwork.show();
				
			}
		});
		
		diagNoNetwork.setNegativeButton("Close JRA", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				finish();
				
			}
		});
	}
	
	
//	private void hugeTask(final String uname, final String pass, final boolean isManual){
//
//		SessionedStringRequest staticArrayRequest = new SessionedStringRequest(
//				Method.GET, 
//				URL.URL_BASE + URL.METHOD_GET_STATIC_ARRAYS, 
//				null, 
//				new Listener<String>() {
//
//					@Override
//					public void onResponse(String response) {
//						
//						
//						try {
//							Log.d("STATUS", "STATIC REQUEST COMPLETE");
//							initStaticData(response);
//							Log.d("STATUS", "STATIC FETCH COMPLETE");
//							login(uname, pass, isManual);
//							
//						} catch (Exception e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//						
//						
//					}
//				}, 
//				
//				new ErrorListener() {
//
//					@Override
//					public void onErrorResponse(VolleyError error) {
//						error.printStackTrace();
//						
//					}
//				}
//				
//				);
//		
//			G.reqQueue.add(staticArrayRequest);
//		
//			progressDialog = ProgressDialog.show(SplashActivity.this, "Signing in", "please wait");
//			
//		
////		
//
//	}
	
	
	
//	private void login(final String uname, final String pass, final boolean isManual){
//		
//		HashMap<String,String> params = new HashMap<String, String>();
//		
//		params.put(URL.VAR_LOGIN_UNAME, uname);
//		params.put(URL.VAR_LOGIN_PASS, pass);
//		params.put("app", "android 1.0.0");
//		
//		
//		
//		SessionedStringRequest loginRequest = new SessionedStringRequest(
//				Method.POST, 
//				URL.URL_BASE + URL.METHOD_LOGIN, 
//				params, 
//				new Listener<String>() {
//
//					@Override
//					public void onResponse(String response) {
//						progressDialog.dismiss();
//						JSONObject responseJson = null;
//						
//						try {
//							responseJson = new JSONObject(response);
//						} catch (JSONException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//						
//						try {
//							if(responseJson.getBoolean("success"))
//							{
//	//							Crouton.makeText(SplashActivity.this, "Success", Style.CONFIRM).show();
//								Log.d("LOGIN", "SUCCESS");
//								progressDialog.dismiss();
//								Storage.setLoggerEmail(getApplicationContext(), uname);
//								Storage.setLoggerPassword(getApplicationContext(), pass);
//								Storage.setLoggedIn(getApplicationContext(), true);
//								startActivity(new Intent(SplashActivity.this, MainActivity.class));
//								finish();
//							}
//							
//							else {
//								progressDialog.dismiss();
//								Crouton.makeText(SplashActivity.this, "Authentication Failure", Style.ALERT).show();
//								if(!isManual) ivLogo.startAnimation(logoTranslate);
//								Log.d("LOGIN", "FAILURE");
//								Log.d("response", response.toString());
//							}
//						} catch (JSONException e) {
//							
//							e.printStackTrace();
//						}
//						
//						
//						
//					}
//				}, 
//				new ErrorListener() {
//
//					@Override
//					public void onErrorResponse(VolleyError error) {
//						progressDialog.dismiss();
//						Crouton.makeText(SplashActivity.this, "Network error, Try again later", Style.ALERT).show();
//						error.printStackTrace();
//					}
//				}
//				
//				
//			);
//		
//		
//		G.reqQueue.add(loginRequest);
//	}
	
	
	private void initStaticData(String json) throws JSONException
	{
		JSONArray jsonArray = null;
		
		try {
			jsonArray = new JSONArray(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		G.staticProfileInfos = new ArrayList<ProfileInfo>();
		
		for(int i=0; i<jsonArray.length(); ++i){
			
			JSONObject jOb = jsonArray.getJSONObject(i);
			
			ProfileInfo pInfo = new ProfileInfo(
					jOb.getString("slug"), 
					jOb.getInt("grp")
					);
			
			JSONArray optionsArray = jOb.getJSONArray("cs");
			
			for(int j=0; j<optionsArray.length(); ++j){
				pInfo.addOption(optionsArray.getString(j));
			}
			
			
			G.staticProfileInfos.add(pInfo);
			
			
		
			
			
			
		}
		
//		for (ProfileInfo p : G.staticProfileInfos) {
//			
//			Log.d("slug", p.getSlug());
//			Log.d("grp", "" + p.getGrp());
//			
//			ArrayList<String> ops = p.getOptions();
//			for (String o : ops) {
//				
//				Log.d("--option", o);
//				
//			}
//			
//		}
		
		
	}
	
	private class HugeAsyncTask extends AsyncTask<Object, String, JSONObject>{

		
		@Override
		protected void onPreExecute() {
			 progressDialog = ProgressDialog.show(SplashActivity.this, "Signing In", "Please Wait");
			super.onPreExecute();
		}
			
		@Override
		protected JSONObject doInBackground(Object... params) {
			
			okUname = (String) params[0];
			okPass = (String) params[1];
			isOkManual = (Boolean) params[2];
			
			String jString = null;
			
			try {
				jString = G.getJsonArray(
						URL.URL_BASE + URL.METHOD_GET_STATIC_ARRAYS, 
						null, 
						false).toString();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			JSONObject loginResponse = null;
			
			try {
				Log.d("STATUS", "STATIC FETCH COMPLETE");
				initStaticData(jString);
				Log.d("STATUS", "STATIC STORAGE COMPLETE");
				
				ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
				postVars.add(new BasicNameValuePair(URL.VAR_LOGIN_UNAME, (String)params[0]));
				postVars.add(new BasicNameValuePair(URL.VAR_LOGIN_PASS, (String)params[1]));
				postVars.add(new BasicNameValuePair("app", "android 1.0.0"));
				
				loginResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_LOGIN, postVars, true);
				
				
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return loginResponse;
			
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			
			progressDialog.dismiss();
			try {
				if(result.getBoolean("success"))
				{
//							Crouton.makeText(SplashActivity.this, "Success", Style.CONFIRM).show();
					Log.d("LOGIN", "SUCCESS");
					progressDialog.dismiss();
					
					G.myId = Integer.parseInt(result.getString("data"));
					
					Storage.setLoggerEmail(getApplicationContext(), okUname);
					Storage.setLoggerPassword(getApplicationContext(), okPass);
					Storage.setLoggedIn(getApplicationContext(), true);
					
					Intent inte = new Intent(SplashActivity.this, MainActivity.class);
								
					inte.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					startActivity(inte);
					overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
					finish();
				}
				
				else {
					progressDialog.dismiss();
					Crouton.makeText(SplashActivity.this, "Authentication Failure", Style.ALERT).show();
					if(!isOkManual) ivLogo.startAnimation(logoTranslate);
					Log.d("LOGIN", "FAILURE");
					Log.d("response", result.toString());
				}
			} catch (JSONException e) {
				
				e.printStackTrace();
			}
		}
			
		}
		
	

	
	

}
