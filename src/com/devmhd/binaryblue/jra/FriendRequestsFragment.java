package com.devmhd.binaryblue.jra;

import java.io.IOException;
import java.util.ArrayList;

import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.ListView;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockFragment;

public class FriendRequestsFragment extends SherlockFragment {

	
	LoadFriendRequestsTask loadFriendRequestsTask;
	ListView lv;
	ProgressDialog pd;
	LinearLayout llNoReqs;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_friend_req, container, false);  
    
    }
    
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	super.onActivityCreated(savedInstanceState);
   
    	
    	llNoReqs = (LinearLayout) getView().findViewById(R.id.llNoReqs);
    	
    	lv = (ListView) getView().findViewById(R.id.lvFriendReqs);
    	
//    	if(Storage.isFriendRequestsLocallyStored(getActivity().getApplicationContext())){
//			if(G.favourites == null){
//				try {
//					FileStorage.loadFavouritesFromFile(getActivity().getApplicationContext());
//				} catch (IOException e) {
//
//					e.printStackTrace();
//				} catch (ClassNotFoundException e) {
//
//					e.printStackTrace();
//				}
//			}
//			
//			populateGrid(G.favourites);
//
//		}
//		
    	
    	
    	
    	loadFriendRequestsTask = new LoadFriendRequestsTask();
    	loadFriendRequestsTask.execute();
    	
    	
    	
    	
    }
    
    
    
    
    
    
    
    
    private class LoadFriendRequestsTask extends AsyncTask<Void, Void, Void>{


		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(getActivity(), "", "Loading Requests...");
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			JSONObject searchResponse = null;

			try {

				searchResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_GET_FRIEND_REQS, null, false);


				G.frequests = new ArrayList<FriendRequest>();

				

					JSONArray reqArray = searchResponse.getJSONArray("contact");

					for(int i=0; i<reqArray.length(); ++i){

						JSONObject request = reqArray.getJSONObject(i);


						G.frequests.add(new FriendRequest(

								Integer.parseInt(request.getString("id")), 
								request.getString("uname"),
								request.getString("avatar")
				
							));

					}



			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Log.d("ALERT_RESPONSE", searchResponse.toString());
			return null;


		}

		protected void onPostExecute(Void resultArray) {




			pd.dismiss();

			populateList();
			

		};

	}
    
    private void populateList(){
    	
    	if(G.frequests.isEmpty()) llNoReqs.setVisibility(View.VISIBLE);
		else{
			lv.setAdapter(new FriendReqListAdapter(getActivity(), G.frequests));
			lv.setVisibility(View.VISIBLE);
		}
    }

    @Override
    public void onDestroy() {
    	if(loadFriendRequestsTask != null) loadFriendRequestsTask.cancel(true);
    	super.onDestroy();
    }


}