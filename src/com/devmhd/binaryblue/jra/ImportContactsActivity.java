package com.devmhd.binaryblue.jra;



import java.util.ArrayList;

import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.ListView;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;


public class ImportContactsActivity extends SherlockActivity {


	private Button btnSelectAll, btnSelectNone;

	private static final int DIALOG_DOWNLOAD_PROGRESS = 0;
	private ProgressDialog mProgressDialog;
	ListView lv;
	ContactListAdapter contactsAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_import_contacts);
		// Show the Up button in the action bar.
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		setTitle("Invite Contacts");

		lv = (ListView) findViewById(R.id.lvImportedContacts);

		G.contactsToInvite = new ArrayList<Contact>();
		contactsAdapter = new ContactListAdapter(getApplicationContext(), G.contactsToInvite);

		lv.setAdapter(contactsAdapter);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int arg2,
					long arg3) {
				int pos = (Integer) v.getTag();

				G.contactsToInvite.get(pos).setSelected(!(G.contactsToInvite.get(pos).isSelected()));
				contactsAdapter.notifyDataSetChanged();
			}
		});



		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			new ContactLoaderTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getApplicationContext());
		} else {
			new ContactLoaderTask().execute(getApplicationContext());
		}


		btnSelectAll = (Button) findViewById(R.id.btnSelectAllContactsInvite);
		btnSelectAll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				for (Contact contact : G.contactsToInvite) {
					contact.setSelected(true);
				}

				contactsAdapter.notifyDataSetChanged();

			}
		});


		btnSelectNone = (Button) findViewById(R.id.btnSelectNoContactsInvite);
		btnSelectNone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				for (Contact contact : G.contactsToInvite) {
					contact.setSelected(false);
				}

				contactsAdapter.notifyDataSetChanged();

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.activity_import_contacts, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}




	public Uri getContactPhotoUri(long contactId) {
		Uri photoUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
		photoUri = Uri.withAppendedPath(photoUri, Contacts.Photo.CONTENT_DIRECTORY);
		return photoUri;
	}

	private class ContactLoaderTask extends AsyncTask<Context, Integer, ArrayList<Contact>> {

		Context ctx;


		@Override
		protected void onPreExecute() {
			showDialog(DIALOG_DOWNLOAD_PROGRESS);

		}

		@Override
		protected ArrayList<Contact> doInBackground(Context... ct) {

			ctx = ct[0];

			ArrayList<Contact> contacts = new ArrayList<Contact>();

		


			ContentResolver cr = getContentResolver();

			Cursor cur = cr.query(Data.CONTENT_URI, new String[] { Data.CONTACT_ID, Data.MIMETYPE, Email.ADDRESS,
					Contacts.DISPLAY_NAME, Phone.NUMBER }, null, null, Contacts.DISPLAY_NAME);


			if (cur.getCount() > 0) {
				
				publishProgress(0, cur.getCount());

				int i = 0;
				while (cur.moveToNext()) {

					String id = cur.getString(cur.getColumnIndex(Data.CONTACT_ID));

				
					Uri photoUri = getContactPhotoUri(Long.parseLong(id));


					String name = cur.getString(cur.getColumnIndex(Contacts.DISPLAY_NAME));

					String email = cur.getString(cur.getColumnIndex(android.provider.ContactsContract.CommonDataKinds.Email.ADDRESS));

					if(email!=null){
						if(isValidEmailAddress(email)){
							
							contacts.add(new Contact(id, name, photoUri, email, false));
							Log.d(G.DEBUG_TAG, name + "  " + photoUri.toString());
						}
						
					} 
					
					publishProgress(1, ++i);

				}
			}

			cur.close();


			return contacts;
		}

		protected void onProgressUpdate(Integer... progress) {

			if(progress[0] == 0)
				mProgressDialog.setMax(progress[1]);
			else
				mProgressDialog.setProgress(progress[1]);
		}


		@Override
		protected void onPostExecute(ArrayList<Contact> contacts) {

			dismissDialog(DIALOG_DOWNLOAD_PROGRESS);

			G.contactsToInvite.addAll(contacts);
			contactsAdapter.notifyDataSetChanged();

			super.onPostExecute(contacts);
		}

	}


	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_DOWNLOAD_PROGRESS:
			mProgressDialog = new ProgressDialog(this);
			mProgressDialog.setMessage("Importing Contacts...");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			return mProgressDialog;
		default:
			return null;
		}
	}

	public boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}



}
