package com.devmhd.binaryblue.jra;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class NavAdapter extends ArrayAdapter<NavItem> {

	Context context;
	
	ArrayList<NavItem> listArray;
	
	
	public NavAdapter(Context context, ArrayList<NavItem> listArray) {
		
		super(context, R.layout.row_nav, listArray);
	
		
		this.context = context;
		
		this.listArray = listArray;
		
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		View v = convertView;
		if(v==null)
		{
			Log.d("NULL", "VIIIEWWWW NNUUULLLLLL");
			LayoutInflater li = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			
			if(listArray.get(position).isHasCount()) v = li.inflate(R.layout.row_nav_with_count, null);
			else v = li.inflate(R.layout.row_nav, null);
		}
		
		TextView tvHeader, tvCount = null;
		
		tvHeader = (TextView) v.findViewById(R.id.tvContent);
		if(listArray.get(position).isHasCount()) tvCount = (TextView) v.findViewById(R.id.tvCount);
	
		
		if (tvHeader!=null) tvHeader.setText(listArray.get(position).getName());
		if(listArray.get(position).isHasCount()) if (tvCount!=null) tvCount.setText("" + listArray.get(position).getCount());
		
		((ImageView) v.findViewById(R.id.ivIcon)).setImageResource(listArray.get(position).getIconRes());
		
		
		//v.setTag(listArray[position].getqId());
		
		return v;
	}
	
	

}
