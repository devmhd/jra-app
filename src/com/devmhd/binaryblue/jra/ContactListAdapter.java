package com.devmhd.binaryblue.jra;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactListAdapter extends ArrayAdapter<Contact> {

	Context context;
	int textViewResourceId;
	ArrayList<Contact> listArray;

	public ContactListAdapter(Context context, 
			ArrayList<Contact> listArray) {
		super(context, R.layout.row_contact_invite, listArray);

		this.context = context;
		this.listArray = listArray;

	}
	
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

	
		
		View v = convertView;
		if (v == null) {
			Log.d("NULL", "VIIIEWWWW NNUUULLLLLL");
			LayoutInflater li = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(R.layout.row_contact_invite, null);
		}

		TextView tvBig = (TextView) v.findViewById(R.id.tvContactNameLabel);
		TextView tvSmall = (TextView) v.findViewById(R.id.tvEmail);
		ImageView iv = (ImageView)v.findViewById(R.id.ivContactImage);
		CheckBox cbSelected = (CheckBox) v.findViewById(R.id.cbIsContactInvited);

		if (tvBig != null) tvBig.setText(listArray.get(position).displayName);

		
		if (tvSmall != null) tvSmall.setText(listArray.get(position).email);
		
		if(iv != null){
			//if(listArray.get(position).photoUri == null) 
			
			//iv.setImageResource(R.drawable.contact_default);
			//else 
			iv.setImageURI(listArray.get(position).photoUri);
			
			if(iv.getDrawable() == null) iv.setImageResource(R.drawable.contact_default);
		} 
		
		
		
		if(cbSelected != null){
			cbSelected.setChecked(listArray.get(position).selected);
		}
			

		v.setTag(position);

		return v;
	}

}
