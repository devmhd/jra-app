package com.devmhd.binaryblue.jra;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.ProgressDialog;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;



public class MapActivity extends SherlockFragmentActivity {

	GoogleMap map;

	ProgressDialog pd;
	SupportMapFragment mapF;
	//	ArrayList<Marker> markers;
	LatLngBounds bounds;
	public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
	private ProgressDialog mProgressDialog;

	HashMap<Marker, MapResult> markerMapping;

	MarkerDrawingTask markerDrawingTask;

	boolean isMultiple;
	boolean haveToLoadBitmaps = true;

	ArrayList<Bitmap> markerThumbs = null;

	String[] cameraOptions = {

			"Show All",
			"Within 10 km",
			"Within 50 km",
			"Within State"
	};



	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_map);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		isMultiple = getIntent().getExtras().getBoolean("multiple");

		setTitle(getIntent().getExtras().getString("title"));

		
		mapF = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)); 
		map = mapF.getMap();


		map.setMyLocationEnabled(true);
		
		

		markerMapping = new HashMap<Marker, MapResult>();

		if(isMultiple) initCameraOptions();


	}


	private void initCameraOptions() {
		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		ArrayAdapter<String> navAdapter = new ArrayAdapter<String>(this, org.holoeverywhere.R.layout.simple_spinner_item_inverse, cameraOptions);

		navAdapter.setDropDownViewResource(org.holoeverywhere.R.layout.simple_spinner_dropdown_item_inverse);

		ActionBar.OnNavigationListener navListener = new ActionBar.OnNavigationListener() {

			@Override
			public boolean onNavigationItemSelected(int itemPosition, long itemId) {

				Location myLocation = map.getMyLocation();
				
				if(myLocation==null){
					Crouton.makeText(MapActivity.this, "Waiting for your location", Style.INFO).show();
					return true;
				}
				
				G.initialPosition = new LatLng(
						myLocation.getLatitude(), 
						myLocation.getLongitude()
						);
				
				if(G.initialPosition == null){
					Crouton.makeText(MapActivity.this, "Your location is not available", Style.ALERT).show();
					return true;
				}
				else{
					
					CameraUpdate cu = null;
					
					if(itemPosition==0) cu = CameraUpdateFactory.newLatLngBounds(bounds, 40);
					if(itemPosition==1) cu = CameraUpdateFactory.newLatLngZoom(G.initialPosition, 12);
					if(itemPosition==2) cu = CameraUpdateFactory.newLatLngZoom(G.initialPosition, 9);
					if(itemPosition==3) cu = CameraUpdateFactory.newLatLngZoom(G.initialPosition, 7);
					
					map.animateCamera(cu);
					
					return true;
				}
			}
		};

		getSupportActionBar().setListNavigationCallbacks(navAdapter, navListener);

	}


	@Override
	protected void onResume() {
		super.onResume();


		markerDrawingTask = new MarkerDrawingTask();


		
		





		markerDrawingTask.execute(isMultiple);

	


		map.setOnInfoWindowClickListener(new InfoListener());

	}






	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getSupportMenuInflater().inflate(R.menu.activity_map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			
			finish();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}




	private class InfoListener implements OnInfoWindowClickListener
	{

		@Override
		public void onInfoWindowClick(Marker marker) {

			if(isMultiple){

				MapResult result = markerMapping.get(marker);

				if (result.getId()!=G.myId) {
					Intent in = new Intent(MapActivity.this,
							ProfileActivity.class);
					in.putExtra("id", result.getId());
					startActivity(in);
				}

			}
		

			else{

				finish();

			}

		}

	}



	private class MarkerDrawingTask extends AsyncTask<Boolean, Integer, ArrayList<MarkerOptions>>{

		@SuppressWarnings("deprecation")
		@Override
		protected void onPreExecute() {
			
			
			super.onPreExecute();
			if(isMultiple){
				showDialog(DIALOG_DOWNLOAD_PROGRESS);
				mProgressDialog.setMax(G.currentMapResults.size());
			}
				
			else
				pd= ProgressDialog.show(MapActivity.this, "", "Finding Location...");
			
		}


		@Override
		protected ArrayList<MarkerOptions> doInBackground(Boolean... params) {


			if(haveToLoadBitmaps) markerThumbs = new ArrayList<Bitmap>();

			ArrayList<MarkerOptions> optionsList = new ArrayList<MarkerOptions>();

			if(params[0]){  //isMultiple

				for (int index=0; index < G.currentMapResults.size(); ++index) {

					MapResult result = G.currentMapResults.get(index);

					
					Bitmap holder = BitmapFactory.decodeResource(getResources(), R.drawable.person_marker_holder); // holder is the frame

					MarkerOptions mOptions = new MarkerOptions();

					mOptions.position(result.getLatLng());
					mOptions.title(result.getName());

					if(index==0)
						bounds = new LatLngBounds(result.getLatLng(), result.getLatLng());
					else
						bounds = bounds.including(result.getLatLng());


					Bitmap.Config conf = Bitmap.Config.ARGB_8888;
					Bitmap bmp = Bitmap.createBitmap(54, 68, conf);
					Canvas canvas1 = new Canvas(bmp);


					Paint color = new Paint();
					color.setTextSize(35);
					color.setColor(Color.BLACK);

					Bitmap thumb;

					if(haveToLoadBitmaps){
						thumb = getBitmapFromURL(result.getAvatar());
						
						markerThumbs.add(thumb);
						
						if(thumb==null) Log.d("Null for", result.getAvatar());
					}

					else thumb = markerThumbs.get(index);

					Log.d("MAP_DRAWING", "Bitmap " + (index+1) + " of " + G.currentMapResults.size() + " loaded");
					publishProgress(index+1);

					int minDimen = Math.min(thumb.getHeight(), thumb.getWidth());

					canvas1.drawBitmap(
							thumb, 
							new Rect(0, 0, minDimen, minDimen),
							new Rect(5, 5, 50, 50),
							color);

					canvas1.drawBitmap(
							holder, 
							new Rect(0, 0, holder.getWidth(), holder.getHeight()),
							new Rect(0, 0, 54, 68),
							color);

					mOptions.icon(BitmapDescriptorFactory.fromBitmap(bmp));

					optionsList.add(mOptions);


					//					Marker mMarker = map.addMarker(mOptions);
					//
					//					markers.add(mMarker);

				}

			}
			else{



				Bitmap holder = BitmapFactory.decodeResource(getResources(), R.drawable.person_marker_holder);

				MarkerOptions mOptions = new MarkerOptions();

				mOptions.position(G.currentProfile.getLatLng());
				mOptions.title(G.currentProfile.getName());

				

				Bitmap.Config conf = Bitmap.Config.ARGB_8888;
				Bitmap bmp = Bitmap.createBitmap(54, 68, conf);
				Canvas canvas1 = new Canvas(bmp);


				Paint color = new Paint();
				color.setTextSize(35);
				color.setColor(Color.BLACK);

				Bitmap thumb = getBitmapFromURL(G.currentProfile.getAvatar());

				int minDimen = Math.min(thumb.getHeight(), thumb.getWidth());

				canvas1.drawBitmap(
						thumb, 
						new Rect(0, 0, minDimen, minDimen),
						new Rect(5, 5, 50, 50),
						color);

				canvas1.drawBitmap(
						holder, 
						new Rect(0, 0, holder.getWidth(), holder.getHeight()),
						new Rect(0, 0, 54, 68),
						color);

				mOptions.icon(BitmapDescriptorFactory.fromBitmap(bmp));

				optionsList.add(mOptions);


				//								Marker mMarker = map.addMarker(mOptions);

				//								markers.add(mMarker);

			}

			haveToLoadBitmaps = false;

			return optionsList;


		}
		
		
		protected void onProgressUpdate(Integer... progress) {
		    mProgressDialog.setProgress(progress[0]);
		}

		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(ArrayList<MarkerOptions> optionsList) {

			if(isMultiple)
				dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
			else
				pd.dismiss();

			for (int i = 0; i < optionsList.size(); ++i) {

				MarkerOptions markerOptions = optionsList.get(i);

				Marker marker = map.addMarker(markerOptions);
				
				

				if(isMultiple) markerMapping.put(marker, G.currentMapResults.get(i));

				//				markers.add();

			}

			CameraUpdate cu;

			if(isMultiple) cu = CameraUpdateFactory.newLatLngBounds(bounds, 40);
			else cu = CameraUpdateFactory.newLatLngZoom(G.currentProfile.getLatLng(), 15);
			map.animateCamera(cu);

		}


	} 




	public static Bitmap getBitmapFromURL(String src) {
		try {		
			java.net.URL url = new java.net.URL(src);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	protected void onPause() {
		markerDrawingTask.cancel(true);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		Crouton.cancelAllCroutons();
		super.onDestroy();
	}


	@Override
	protected Dialog onCreateDialog(int id) {
	    switch (id) {
	    case DIALOG_DOWNLOAD_PROGRESS:
	        mProgressDialog = new ProgressDialog(this);
	        mProgressDialog.setMessage("Loading people...");
	        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	        mProgressDialog.setCancelable(false);
	        mProgressDialog.show();
	        return mProgressDialog;
	    default:
	    return null;
	    }
	}


}
