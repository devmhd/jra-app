package com.devmhd.binaryblue.jra;

public class URL {

	public static final String URL_BASE = "https://juicyredapple.com.au/";
	
	public static final String URL_SIGNUP = "https://juicyredapple.com.au";
	
	
	public static final String METHOD_LOGIN = "login";
	public static final String METHOD_GET_STATIC_ARRAYS = "src/js/opts.json";
	public static final String METHOD_GET_MY_PROFILE = "getfullprofile";
	public static final String METHOD_SUBMIT_SIMPLE_SEARCH = "searchmembers";
	public static final String METHOD_GET_PROFILE = "getprofiles";
	public static final String METHOD_SUBMIT_LOCATION_UPDATE = "index.php/jra_android/update_cur_loc";
	
	public static final String METHOD_GET_JRA_LIVE = "index.php/jra_android/get_cur_loc_all";
	public static final String METHOD_GET_CONTACTS = "getcontacts-app";
	public static final String METHOD_GET_FAVOURITES = "getfavourites-app";
	public static final String METHOD_ADD_CONTACT = "index.php/profile/add_friend_list";
	public static final String METHOD_ADD_FAVOURITE = "index.php/profile/add_fav_list";
	public static final String METHOD_SEND_MESSAGE = "index.php/dashboard/reply_msg";
	
	public static final String METHOD_GET_FRIEND_REQS = "index.php/profile/alert_contact";
	
	public static final String METHOD_GET_INBOX = "index.php/dashboard/my_old_message_details";
	
	public static final String METHOD_ACCEPT_FRIEND_REQ = "index.php/user/accept_contact_req";
	public static final String METHOD_DENY_FRIEND_REQ = "index.php/user/deny_contact_req";
	
	
	
	public static final String VAR_LOGIN_UNAME = "jname";
	public static final String VAR_LOGIN_PASS = "pass";
	
	public static final String VAR_SIMPLE_SEARCH = "search";
	
	public static final String VAR_GET_PROFILE = "ids[0]";
	
	public static final String VAR_MY_LOCATION_LAT = "lat";
	public static final String VAR_MY_LOCATION_LNG = "lan";
	
	public static final String VAR_ADD_CONTACT_ID = "two";
	
	public static final String VAR_ADD_FAVOURITE = "two";
	
	
	public static final String VAR_SEND_MESSAGE_BODY =  "reply_msg_txt";
	
	public static final String VAR_SEND_MESSAGE_ID = "hidden_id_msg";
	
	public static final String VAR_ACCEPT_FRIEND_REQ_UID = "idd";
	public static final String VAR_DENY_FRIEND_REQ_UID = "idd";
	
	public static final String VAR_SIMPLE_SEARCH_IS_UNAME = "isun";
	
	
	
	
}
