package com.devmhd.binaryblue.jra;

public class ProfileAttribute {

	private String name;
	private String value;
	
	private int group;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public ProfileAttribute(String name, String value, int group) {
		super();
		this.name = name;
		this.value = value;
		this.group = group;
	}
	
	
	
	
}
