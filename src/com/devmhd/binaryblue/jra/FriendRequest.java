package com.devmhd.binaryblue.jra;

import java.io.Serializable;

public class FriendRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 34L;
	private int uid;
	private String name;
	private String avatar;
	private int state;
	
	
	
	public FriendRequest(int uid, String name, String avatar) {
		super();
		this.uid = uid;
		this.name = name;
		this.avatar = avatar;
		state = 0;
	}


	public void accept(){
		state = 1;
	}

	
	public void deny(){
		state = 2;
	}


	public int getUid() {
		return uid;
	}





	public String getName() {
		return name;
	}


	


	public String getAvatar() {
		return avatar;
	}


	


	public int getState() {
		return state;
	}


	
	
	
	
}
