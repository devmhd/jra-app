package com.devmhd.binaryblue.jra;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class Profile {
	
	private String name;
	private String status;
	private String about;
	private String location;
	private String avatar;
	private String age;
	private LatLng latLng;
	private int appleMatch;
	private int id;
	
	private boolean friend;
	private boolean fav;
	
	
	
	public boolean isFriend() {
		return friend;
	}

	public boolean isFav() {
		return fav;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LatLng getLatLng() {
		return latLng;
	}

	public int getAppleMatch() {
		return appleMatch;
	}

	public String getAge() {
		return age;
	}

	private ArrayList<ProfileAttribute> attributes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public ArrayList<ProfileAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(ArrayList<ProfileAttribute> attributes) {
		this.attributes = attributes;
	}

	public Profile(JSONObject json) {
		
		attributes = new ArrayList<ProfileAttribute>();
		
		try {
			
			name = json.getJSONArray("data").getJSONObject(0).getString("uname");
			status = json.getJSONArray("data").getJSONObject(0).getString("status");
			about = json.getJSONArray("data").getJSONObject(0).getString("about");
			location = json.getJSONArray("data").getJSONObject(0).getString("location");
			avatar = json.getJSONArray("data").getJSONObject(0).getString("avatar");
			age = json.getJSONArray("data").getJSONObject(0).getString("age");
			
			friend = json.getJSONArray("data").getJSONObject(0).getBoolean("friend");
			fav = json.getJSONArray("data").getJSONObject(0).getBoolean("fav");
			
			appleMatch =Integer.parseInt(json.getJSONArray("data").getJSONObject(0).getString("app_match"));
			
			latLng = new LatLng(
					Double.parseDouble(json.getJSONArray("data").getJSONObject(0).getString("lat")), 
					Double.parseDouble(json.getJSONArray("data").getJSONObject(0).getString("lng")));
			
			
			JSONArray optionsArray = json.getJSONArray("data").getJSONObject(0).getJSONArray("bits");
			
			for(int j=0; j<optionsArray.length(); ++j){
				//pInfo.addOption();
				
				int bit = Integer.parseInt(optionsArray.getString(j));
				
				int index = log2(bit);
				
				String name = G.staticProfileInfos.get(j).getSlug();
				
				String value = G.staticProfileInfos.get(j).getOptions().get(index);
				
				attributes.add(new ProfileAttribute(name, value, G.staticProfileInfos.get(j).getGrp()));
				
				Log.d(""+G.staticProfileInfos.get(j).getGrp()+name, value);
			}
			
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	static int log2(int x)
	{
	    return (int) (Math.log(x) / Math.log(2));
	}
	
	

}
