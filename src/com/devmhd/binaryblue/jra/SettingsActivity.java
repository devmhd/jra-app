package com.devmhd.binaryblue.jra;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.CheckBox;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SettingsActivity extends SherlockActivity {


	LinearLayout llKeepMeLiveMap, llRemindMeAfter;

	TextView tvKeepMeAliveValue, tvRemindmeAfterValue;
	
	Button btnInviteContacts;

	CheckBox cbChat, cbNotification;

	CharSequence[] keepMeAliveOptions = new CharSequence[] {"Inside JRA app", "Always", "Manual", "Never"};
	CharSequence[] RemindMeOptions = new CharSequence[] {"30 minutes", "1 Hour", "2 Hours", "24 Hours", "7 Days", "30 Days"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		setTitle("JRA Settings");

		llKeepMeLiveMap = (LinearLayout) findViewById(R.id.llKeepMeLive);
		llRemindMeAfter = (LinearLayout) findViewById(R.id.llRemindMeAfter);

		tvKeepMeAliveValue = (TextView) findViewById(R.id.lblKeepMeLiveValue);
		tvRemindmeAfterValue = (TextView) findViewById(R.id.lblRemindMeAfterValue);

		cbChat = (CheckBox) findViewById(R.id.cbChat);
		cbNotification = (CheckBox) findViewById(R.id.cbNotification);
		
		btnInviteContacts = (Button) findViewById(R.id.btnInviteContacts);
		
		btnInviteContacts.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				startActivity(new Intent(SettingsActivity.this, ImportContactsActivity.class));
				
			}
		});

		loadPrefs();

		llKeepMeLiveMap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showKeepMeAliveDialog();

			}
		});

		llRemindMeAfter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showReindMeDialog();

			}
		});


		cbNotification.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean checked) {


				Storage.setNotificationOn(getApplicationContext(), checked);
				

			}
		});


		cbChat.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean checked) {


				Storage.setChatOn(getApplicationContext(), checked);
					

			}
		});

	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	private void loadPrefs(){


		int keepme = Storage.getKeepMeAliveValue(getApplicationContext());
		tvKeepMeAliveValue.setText(keepMeAliveOptions[keepme]);

		if(keepme == 3){
			llRemindMeAfter.setVisibility(View.VISIBLE);

		}else{
			llRemindMeAfter.setVisibility(View.GONE);
		}
		
		tvRemindmeAfterValue.setText(RemindMeOptions[Storage.getRemindMeValue(getApplicationContext())]);
		
		
		cbChat.setChecked(Storage.isChatOnStored(getApplicationContext()));
		cbNotification.setChecked(Storage.isNotificationOn(getApplicationContext()));

	}


	private void showKeepMeAliveDialog(){

		CharSequence colors[] = keepMeAliveOptions;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Keep me live on map");
		builder.setItems(colors, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Storage.setKeepMeAliveValue(getApplicationContext(), which);
				tvKeepMeAliveValue.setText(keepMeAliveOptions[which]);

				if(which == 3){
					llRemindMeAfter.setVisibility(View.VISIBLE);

				}else{
					llRemindMeAfter.setVisibility(View.GONE);
				}

			}
		});
		builder.show();

	}



	private void showReindMeDialog(){

		CharSequence colors[] = RemindMeOptions;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Remind me after");
		builder.setItems(colors, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				Storage.setRemindMeValue(getApplicationContext(), which);
				tvRemindmeAfterValue.setText(RemindMeOptions[which]);

			}
		});
		builder.show();

	}



}
