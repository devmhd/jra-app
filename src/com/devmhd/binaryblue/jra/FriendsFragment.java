package com.devmhd.binaryblue.jra;

import java.io.IOException;
import java.util.ArrayList;

import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.GridView;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockFragment;


public class FriendsFragment extends SherlockFragment {


	ProgressDialog pd;
	GridView personsGrid;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_friends, container, false);  

	}

	LoadFriendsTask loadFriendsTask;



	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);


		if(Storage.isFriendsLocallyStored(getActivity().getApplicationContext())){
			if(G.contacts == null){
				try {
					FileStorage.loadFriendsFromFile(getActivity().getApplicationContext());
				} catch (IOException e) {

					e.printStackTrace();
				} catch (ClassNotFoundException e) {

					e.printStackTrace();
				}
			}

			populateGrid(G.contacts);

		}



		loadFriendsTask = new LoadFriendsTask();


		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			loadFriendsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			loadFriendsTask.execute();
		}




	}


	private void populateGrid(ArrayList<SearchResult> friends) {



		personsGrid = (GridView) getView().findViewById(R.id.gridViewNewPeople);

		LinearLayout llNoResults = (LinearLayout) getView().findViewById(R.id.llNoResult);

		if(friends.isEmpty()) llNoResults.setVisibility(View.VISIBLE);
		else personsGrid.setVisibility(View.VISIBLE);

		personsGrid.setAdapter(new PersonGridAdapter(getActivity(), friends));

		personsGrid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View row, int arg2,
					long arg3) {

				Intent in = new Intent(new Intent(getActivity(), ProfileActivity.class));

				int id = (Integer)row.getTag();
				in.putExtra("id", id);
				startActivity(in);

			}
		});


	}





	private class LoadFriendsTask extends AsyncTask<Void, Void, ArrayList<SearchResult>>{


		@Override
		protected void onPreExecute() {
			if(G.contacts == null)
				pd = ProgressDialog.show(getActivity(), "", "Loading Contacts...");
			super.onPreExecute();
		}

		@Override
		protected ArrayList<SearchResult> doInBackground(Void... params) {

			JSONObject searchResponse = null;
			ArrayList<SearchResult> fetchedresults = null;

			try {

				Log.d(G.DEBUG_TAG, "Fired request: " + URL.URL_BASE + URL.METHOD_GET_CONTACTS);
				searchResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_GET_CONTACTS, null, false);


				fetchedresults = new ArrayList<SearchResult>();

				if(searchResponse.getBoolean("success")){

					JSONArray resultArray = searchResponse.getJSONObject("data").getJSONArray("dtls");

					for(int i=0; i<resultArray.length(); ++i){

						JSONObject singleResult = resultArray.getJSONObject(i);






						fetchedresults.add(new SearchResult(

								Integer.parseInt(singleResult.getString("id")), 
								singleResult.getString("about"),
								singleResult.getString("avatar"),
								singleResult.getString("uname"),

								Integer.parseInt(singleResult.getString("app_match"))

								));


					}


				}






			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return fetchedresults;


		}

		protected void onPostExecute(ArrayList<SearchResult> fetchedresults) {

			if(pd!=null) pd.dismiss();

			if(fetchedresults.equals(G.contacts))
				Log.d(G.DEBUG_TAG, "Contacts Fetch complete, No need to update");
			else{

				if(G.contacts == null){
					G.contacts = fetchedresults;
					populateGrid(G.contacts);
					Log.d(G.DEBUG_TAG, "Contacts Fetch complete, UPDATED (Brandnew)");

					try {
						FileStorage.writeFriendsToFile(getActivity().getApplicationContext());
						Storage.setFriendsLocallyStored(getActivity().getApplicationContext(), true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					G.contacts = fetchedresults;
					Log.d(G.DEBUG_TAG, "Contacts Fetch complete, UPDATED");

					try {
						FileStorage.writeFriendsToFile(getActivity().getApplicationContext());
						Storage.setFriendsLocallyStored(getActivity().getApplicationContext(), true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}


			}

		};

	}

	@Override
	public void onDestroy() {
		if(loadFriendsTask!=null) loadFriendsTask.cancel(true);
		super.onDestroy();
	}



}