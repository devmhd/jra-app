package com.devmhd.binaryblue.jra;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.GridView;
import org.holoeverywhere.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.SherlockFragment;

public class HomeFragment extends SherlockFragment {


	ProgressDialog pd;
	GridView personsGrid;



	SimpleSearchTask simpleSearchTask;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_home, container, false);  

	}

	
	
	


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		Log.d(G.DEBUG_TAG, "onActivityCreated");


		if(G.allResults == null){

			simpleSearchTask = new SimpleSearchTask();

			//simpleSearchTask.execute();

				
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				simpleSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "","0");
			} else {
				simpleSearchTask.execute("","0");
			}
	

		}

		else 
		{
			populateGrid(G.allResults);

		}


	}



	@Override
	public void onDestroy() {
		if(simpleSearchTask !=null) simpleSearchTask.cancel(false);
		super.onDestroy();
	}


	private class SimpleSearchTask extends AsyncTask<String, Void, JSONObject>{


		@Override
		protected void onPreExecute() {

			if(!(Storage.isSearchResultsLocallyStored(getActivity().getApplicationContext())))
				pd = ProgressDialog.show(getActivity(), "", "Let's find compatible people for you...\nThis might take a minute");
			else{
				try {
					FileStorage.loadSearchResultsFromFile(getActivity().getApplicationContext());
					populateGrid(G.allResults);

				} catch (IOException e) {

					e.printStackTrace();
				} catch (ClassNotFoundException e) {

					e.printStackTrace();
				}
			}
			super.onPreExecute();
		}

		@Override
		protected JSONObject doInBackground(String... params) {

			JSONObject searchResponse = null;

			try {

				ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
				postVars.add(new BasicNameValuePair(URL.VAR_SIMPLE_SEARCH, (String)params[0]));
				postVars.add(new BasicNameValuePair(URL.VAR_SIMPLE_SEARCH_IS_UNAME, (String)params[1]));

				Log.d(G.DEBUG_TAG, "Request Fired: " + URL.URL_BASE + URL.METHOD_SUBMIT_SIMPLE_SEARCH + " " + postVars.toString() );

				searchResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_SUBMIT_SIMPLE_SEARCH, postVars, true);




			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		//	Log.d(G.DEBUG_TAG, "SEARCH_RESPONSE: " +  searchResponse.toString());
			return searchResponse;


		}

		protected void onPostExecute(JSONObject searchResponse) {

			Log.d(G.DEBUG_TAG, "Fetched new homepage results");

			try {
				if(searchResponse.getBoolean("success")){

					JSONArray resultArray = searchResponse.getJSONObject("data").getJSONArray("res");




					ArrayList<SearchResult> fetchedResults = new ArrayList<SearchResult>();
					for(int i=0; i<resultArray.length(); ++i){
						JSONObject result =  resultArray.getJSONObject(i);

						fetchedResults.add(new SearchResult(
								Integer.parseInt(result.getString("id")), 
								result.getString("about"), 
								result.getString("avatar"), 
								result.getString("uname"),
								Integer.parseInt(result.getString("app_match"))

								));


					}

					if(G.allResults != null){

						if(G.allResults.equals(fetchedResults))
							Log.d(G.DEBUG_TAG, "No need to update homepage results");
						else{
							G.allResults = fetchedResults;
							Log.d(G.DEBUG_TAG, "Homepage results updated");

							//populateGrid(G.allResults);

							try {
								FileStorage.writeSearchResultsToFile(getActivity().getApplicationContext());
								Storage.setSearchResultsLocallyStored(getActivity().getApplicationContext(), true);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}else{

						G.allResults = fetchedResults;
						//populateGrid(G.allResults);

						try {
							FileStorage.writeSearchResultsToFile(getActivity().getApplicationContext());
							Storage.setSearchResultsLocallyStored(getActivity().getApplicationContext(), true);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}


					}



				}

				else G.allResults = null;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(pd!=null){
				pd.dismiss();
				populateGrid(G.allResults);
			} 

			//startActivity(new Intent(getActivity(), SearchResultActivity.class));










		};

	}


	private void populateGrid(ArrayList<SearchResult> results) {


		View fragmentView = getView();

		if(fragmentView==null) return;

		GridView lv = (GridView) fragmentView.findViewById(R.id.gridViewNewPeople);


		if(lv==null){
			Log.d(G.DEBUG_TAG, "SearchListView NULL");
			return;
		} 


		lv.setAdapter(new PersonGridAdapter(getActivity(), G.allResults));


		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View row, int arg2,
					long arg3) {

				Intent in = new Intent(new Intent(getActivity(), ProfileActivity.class));

				int id = (Integer)row.getTag();
				in.putExtra("id", id);
				startActivity(in);

			}
		});

	}




}