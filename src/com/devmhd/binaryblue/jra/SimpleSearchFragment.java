package com.devmhd.binaryblue.jra;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.holoeverywhere.ArrayAdapter;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.Spinner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class SimpleSearchFragment extends SherlockFragment {


	ProgressDialog pd;
	Spinner spSimpleSearch;
	
	String[] simpleSearchOpts = {
			
			"Search by Full Name",
			"Search by Username"
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_search_simple, container, false);  

	}



	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	
		
		
		
		
		spSimpleSearch = (Spinner) getView().findViewById(R.id.spSearchSimple);
		
		ArrayAdapter<String> spAdapter = new ArrayAdapter<String>(getActivity(), org.holoeverywhere.R.layout.simple_spinner_item, simpleSearchOpts);
		spAdapter.setDropDownViewResource(org.holoeverywhere.R.layout.simple_spinner_dropdown_item);
		
		spSimpleSearch.setAdapter(spAdapter);

		((Button) getView().findViewById(R.id.btnSearch)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				
				
				String queryString = ((EditText) getView().findViewById(R.id.etQueryString)).getText().toString();
				
				if(queryString.equals("")){
					Crouton.makeText(getActivity(), "Please give a valid name", Style.ALERT).show();
					return;
				}
				
				String isun = "" + spSimpleSearch.getSelectedItemPosition();
				
				new SimpleSearchTask().execute(queryString, isun);




			}
		});


	}

	private class SimpleSearchTask extends AsyncTask<String, Void, JSONObject>{


		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(getActivity(), "", "Searching...");
			super.onPreExecute();
		}

		@Override
		protected JSONObject doInBackground(String... params) {

			JSONObject searchResponse = null;

			try {

				ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
				postVars.add(new BasicNameValuePair(URL.VAR_SIMPLE_SEARCH, (String)params[0]));
				postVars.add(new BasicNameValuePair(URL.VAR_SIMPLE_SEARCH_IS_UNAME, (String)params[1]));
				searchResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_SUBMIT_SIMPLE_SEARCH, postVars, true);




			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Log.d("SEARCH_RESPONSE", searchResponse.toString());
			return searchResponse;


		}

		protected void onPostExecute(JSONObject searchResponse) {

			
			try {
				if(searchResponse.getBoolean("success")){
					
					JSONArray resultArray = searchResponse.getJSONObject("data").getJSONArray("res");
					
					G.currentSearchResults = new ArrayList<SearchResult>();
					for(int i=0; i<resultArray.length(); ++i){
						JSONObject result =  resultArray.getJSONObject(i);
						
						G.currentSearchResults.add(new SearchResult(
								Integer.parseInt(result.getString("id")), 
								result.getString("about"), 
								result.getString("avatar"), 
								result.getString("uname"),
								Integer.parseInt(result.getString("app_match"))
								
								));
						
					}
					
				}
				
				else G.currentSearchResults = null;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			pd.dismiss();
			
			startActivity(new Intent(getActivity(), SearchResultActivity.class));

		};

	}




}