package com.devmhd.binaryblue.jra;

import java.io.IOException;
import java.util.ArrayList;

import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.GridView;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockFragment;

public class FavouritesFragment extends SherlockFragment {


	ProgressDialog pd;
	GridView personsGrid;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_favorites, container, false);  

	}

	LoadFavouritesTask loadFavouritesTask;


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);


		if(Storage.isFavouritesLocallyStored(getActivity().getApplicationContext())){
			if(G.favourites == null){
				try {
					FileStorage.loadFavouritesFromFile(getActivity().getApplicationContext());
				} catch (IOException e) {

					e.printStackTrace();
				} catch (ClassNotFoundException e) {

					e.printStackTrace();
				}
			}
			
			populateGrid(G.favourites);

		}
		
		

		loadFavouritesTask = new LoadFavouritesTask();


		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			loadFavouritesTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			loadFavouritesTask.execute();
		}

	}


	private void populateGrid(ArrayList<SearchResult> friends) {

		View view = getView();
		if(view == null) return;

		personsGrid = (GridView) view.findViewById(R.id.gridViewNewPeople);

		LinearLayout llNoResults = (LinearLayout) getView().findViewById(R.id.llNoResult);

		if(friends.isEmpty()) llNoResults.setVisibility(View.VISIBLE);
		else personsGrid.setVisibility(View.VISIBLE);

		personsGrid.setAdapter(new PersonGridAdapter(getActivity(), friends));


		personsGrid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View row, int arg2,
					long arg3) {

				Intent in = new Intent(new Intent(getActivity(), ProfileActivity.class));

				int id = (Integer)row.getTag();
				in.putExtra("id", id);
				startActivity(in);

			}
		});


	}


	private class LoadFavouritesTask extends AsyncTask<Void, Void, ArrayList<SearchResult>>{


		@Override
		protected void onPreExecute() {

			if(G.favourites == null)
				pd = ProgressDialog.show(getActivity(), "", "Loading Favourites...");
			super.onPreExecute();
		}

		@Override
		protected ArrayList<SearchResult> doInBackground(Void... params) {

			JSONObject searchResponse = null;
			ArrayList<SearchResult> fetchedresults = null;

			try {

				Log.d(G.DEBUG_TAG, "Fired request: " + URL.URL_BASE + URL.METHOD_GET_FAVOURITES);
				searchResponse = G.getJsonObject(URL.URL_BASE + URL.METHOD_GET_FAVOURITES, null, false);


				fetchedresults = new ArrayList<SearchResult>();

				if(searchResponse.getBoolean("success")){

					JSONArray resultArray = searchResponse.getJSONObject("data").getJSONArray("dtls");

					for(int i=0; i<resultArray.length(); ++i){

						JSONObject singleResult = resultArray.getJSONObject(i);






						fetchedresults.add(new SearchResult(

								Integer.parseInt(singleResult.getString("id")), 
								singleResult.getString("about"),
								singleResult.getString("avatar"),
								singleResult.getString("uname"),

								Integer.parseInt(singleResult.getString("app_match"))

								));




					}


				}






			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return fetchedresults;


		}

		protected void onPostExecute(ArrayList<SearchResult> fetchedresults) {


			if(pd!=null) pd.dismiss();

			if(fetchedresults.equals(G.favourites))
				Log.d(G.DEBUG_TAG, "Favourites Fetch complete, No need to update");
			else{

				if(G.favourites == null){
					G.favourites = fetchedresults;
					populateGrid(G.favourites);
					Log.d(G.DEBUG_TAG, "Favourites Fetch complete, UPDATED (Brandnew)");

					try {
						FileStorage.writeFavouritesToFile(getActivity().getApplicationContext());
						Storage.setFavouritesLocallyStored(getActivity().getApplicationContext(), true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					G.favourites = fetchedresults;
					Log.d(G.DEBUG_TAG, "Favourites Fetch complete, UPDATED");

					try {
						FileStorage.writeFavouritesToFile(getActivity().getApplicationContext());
						Storage.setFavouritesLocallyStored(getActivity().getApplicationContext(), true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}


			}


		};

	}

	@Override
	public void onDestroy() {
		if(loadFavouritesTask!=null) loadFavouritesTask.cancel(true);
		super.onDestroy();
	}



}