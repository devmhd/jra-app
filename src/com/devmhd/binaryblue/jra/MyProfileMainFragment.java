package com.devmhd.binaryblue.jra;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public class MyProfileMainFragment extends SherlockFragment {

	
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_myprofile_main, container, false);  
    
}
    
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	super.onActivityCreated(savedInstanceState);
   
    	
    	
    	
    	
    	
    	((TextView) getView().findViewById(R.id.tvStaticAboutHeader)).setTypeface(G.robotoLight);
    	((TextView) getView().findViewById(R.id.tvStaticStatusHeader)).setTypeface(G.robotoLight);
    	
//    	((ImageButton) getView().findViewById(R.id.btnShowOnMap)).setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				Intent in = new Intent(getActivity(), MapActivity.class);
//				in.putExtra("multiple", false);
//				startActivity(in);
//				
//			}
//		});
    	
    	
    	
    	((TextView) getView().findViewById(R.id.tvName)).setText(G.me.getName());
    	((TextView) getView().findViewById(R.id.tvLocation)).setText(G.me.getLocation());
    	((TextView) getView().findViewById(R.id.tvStatus)).setText(G.me.getStatus());
    	((TextView) getView().findViewById(R.id.tvAbout)).setText(G.me.getAbout());
    	((TextView) getView().findViewById(R.id.tvAge)).setText(G.me.getAge());
    	
    	((SquareImageView) getView().findViewById(R.id.nivMyProfilePic)).setImageUrl(G.me.getAvatar(), G.imageLoader);
    	
    	
    }



}