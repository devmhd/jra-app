package com.devmhd.binaryblue.jra;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.EditText;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public class InboxConversationsAdapter extends ArrayAdapter<Conversation> {

	Context context;

	ArrayList<Conversation> listArray;
	
	ProgressDialog progressDialog;
	
	private AlertDialog.Builder addFriendDialogBuilder;
	private AlertDialog addFriendDiag;
	
	
	public InboxConversationsAdapter(Context context, ArrayList<Conversation> listArray) {
		
		super(context, R.layout.row_conversation_in, listArray);
	
		
		this.context = context;
		
		this.listArray = listArray;

		
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		
		
		
		
		View v = convertView;
		
		final Conversation thisConv = listArray.get(position);
		
		if(v==null)
		{
			Log.d("NULL", "VIIIEWWWW NNUUULLLLLL");
			LayoutInflater li = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			
			if(thisConv.getReceiverId()==G.myId) // InMessage
				v = li.inflate(R.layout.row_conversation_in, null);
			else 
				v = li.inflate(R.layout.row_conversation_out, null);
		}
		
		TextView tvName, tvMessage;
		ImageButton btnReply;
		SquareImageView ivThumb;
		

		
		tvName = (TextView) v.findViewById(R.id.tvSenderName);
		
		if(thisConv.getReceiverId()==G.myId) // InMessage
			tvName.setText(listArray.get(position).getSenderName());
		else 
			tvName.setText(listArray.get(position).getReceiverName());
		
		tvMessage = (TextView) v.findViewById(R.id.tvMessage);
		tvMessage.setText(listArray.get(position).getMsgBody());
		
		ivThumb = (SquareImageView) v.findViewById(R.id.ivMessageThumb);
		
		if(thisConv.getReceiverId()==G.myId) // InMessage
			ivThumb.setImageUrl(listArray.get(position).getSenderAvatar(), G.imageLoader);
		else 
			ivThumb.setImageUrl(listArray.get(position).getReceiverAvatar(), G.imageLoader);
		
		
		ivThumb.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(context, ProfileActivity.class);
				
				if(thisConv.getReceiverId()==G.myId) // InMessage
					intent.putExtra("id", listArray.get(position).getSenderId());
				else
					intent.putExtra("id", listArray.get(position).getReceiverId());
				
				context.startActivity(intent);
				
			}
		});
		
		
		
		
		if (thisConv.getReceiverId()==G.myId) {
			btnReply = (ImageButton) v.findViewById(R.id.btnMsgReply);
			
			
			
			if (btnReply != null) {
				btnReply.setTag(listArray.get(position).getSenderId());
				btnReply.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						int id = (Integer) v.getTag();

						initDiag(listArray.get(position).getSenderName(), id);

						addFriendDiag.show();

					}
				});
			}
		}
		return v;
	}
	
	
	 private void initDiag(String uname,  final int uid){
			addFriendDialogBuilder = new AlertDialog.Builder(context);		
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			final View diagView = inflater.inflate(R.layout.dialog_send_message, null);
			addFriendDialogBuilder.setView(diagView);
			addFriendDialogBuilder.setTitle("Reply to " + uname);
			addFriendDialogBuilder.setPositiveButton("Send Message", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					String message = ((EditText) diagView.findViewById(R.id.etMessage)).getText().toString();
					
					new SendMessageTask().execute(message, "" + uid);
					
					//Crouton.makeText((Activity) context, "Reply Sent", Style.CONFIRM).show();

				}
			});

			addFriendDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					addFriendDiag.cancel();

				}
			});

			addFriendDiag = addFriendDialogBuilder.create();
			
	    }
	 
	 
	 
	 private class SendMessageTask extends AsyncTask<String,Void, String>{

			@Override
			protected void onPreExecute() {
				progressDialog = ProgressDialog.show(context, "", "Sending Message...");
				super.onPreExecute();
			}

			@Override
			protected String doInBackground(String... params) {

				String profileJson = null;

				try {

					ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
					postVars.add(new BasicNameValuePair(URL.VAR_SEND_MESSAGE_BODY, params[0]));
					postVars.add(new BasicNameValuePair(URL.VAR_SEND_MESSAGE_ID, ""+params[1]));

					profileJson = G.getString(URL.URL_BASE + URL.METHOD_SEND_MESSAGE, postVars, true);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return profileJson;
			}
			@Override
			protected void onPostExecute(String result) {
				progressDialog.dismiss();

				Crouton.makeText((Activity) context, result, Style.CONFIRM).show();
				super.onPostExecute(result);


			}

		}
	

}
