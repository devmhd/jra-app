package com.devmhd.binaryblue.jra;

import java.io.Serializable;

public class SearchResult implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int personId;
	private String about;
	private String avatar;
	private String name;
	private int appCount;
	
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAppCount() {
		return appCount;
	}
	public void setAppCount(int appCount) {
		this.appCount = appCount;
	}
	public SearchResult(int personId, String about, String avatar, String name,
			int appCount) {
		super();
		this.personId = personId;
		this.about = about;
		this.avatar = avatar;
		this.name = name;
		this.appCount = appCount;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((about == null) ? 0 : about.hashCode());
		result = prime * result + appCount;
		result = prime * result + ((avatar == null) ? 0 : avatar.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + personId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchResult other = (SearchResult) obj;
		
		if (personId != other.personId)
			return false;
		return true;
	}
	
	

}
