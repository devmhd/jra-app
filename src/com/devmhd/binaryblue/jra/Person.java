package com.devmhd.binaryblue.jra;

import com.google.android.gms.maps.model.LatLng;

public class Person {
	
	private String name;
	private int thumbResId;
	private LatLng position;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getThumbResId() {
		return thumbResId;
	}
	public void setThumbResId(int thumbResId) {
		this.thumbResId = thumbResId;
	}
	public LatLng getPosition() {
		return position;
	}
	public void setPosition(LatLng position) {
		this.position = position;
	}
	public Person(String name, int thumbResId, LatLng position) {
		super();
		this.name = name;
		this.thumbResId = thumbResId;
		this.position = position;
	}
	

	
	
}
