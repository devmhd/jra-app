package com.devmhd.binaryblue.jra;

import java.util.ArrayList;



import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class SearchResultAdapter extends ArrayAdapter<SearchResult> {

	Context context;
	
	ArrayList<SearchResult> listArray;
	
	
	
	public SearchResultAdapter(Context context, ArrayList<SearchResult> listArray) {
		
		super(context, R.layout.row_search_result, listArray);
	
		
		this.context = context;
		
		this.listArray = listArray;

		
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		
		
		
		View v = convertView;
		
		if(v==null)
		{
			Log.d("NULL", "VIIIEWWWW NNUUULLLLLL");
			LayoutInflater li = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			
			
			v = li.inflate(R.layout.row_search_result, null);
		}
		
		TextView tvName, tvStatus;
		SquareImageView nivThumb;
		ImageView ivRating;
		
		
		
		tvName = (TextView) v.findViewById(R.id.tvName);
		tvName.setText(listArray.get(position).getName());
		
		tvStatus = (TextView) v.findViewById(R.id.tvStatus);
		tvStatus.setText(listArray.get(position).getAbout());
		
		nivThumb = (SquareImageView) v.findViewById(R.id.ivThumb);
		nivThumb.setImageUrl(listArray.get(position).getAvatar(), G.imageLoader);
		
		ivRating = (ImageView) v.findViewById(R.id.ivRating);
		ivRating.setImageResource(G.getRatingImg(listArray.get(position).getAppCount()));
		
		v.setTag(listArray.get(position).getPersonId());
		
		return v;
	}
	
	

}
