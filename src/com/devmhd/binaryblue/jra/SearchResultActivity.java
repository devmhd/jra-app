package com.devmhd.binaryblue.jra;


import org.holoeverywhere.widget.ListView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class SearchResultActivity extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_result);
		
		setTitle("Search results");
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		ListView lv = (ListView) findViewById(R.id.lvSearchResult);
		
	
		if(lv==null) Log.d("SearchListView", "NULL");
		if(G.currentSearchResults==null){
			
			return;
		}
		
		((LinearLayout) findViewById(R.id.llNoResult)).setVisibility(View.GONE);
		
		lv.setVisibility(View.VISIBLE);
		lv.setAdapter(new SearchResultAdapter(getApplicationContext(), G.currentSearchResults));
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View row, int arg2,
					long arg3) {
				
				Intent in = new Intent(new Intent(SearchResultActivity.this, ProfileActivity.class));
				
				int id = (Integer)row.getTag();
				in.putExtra("id", id);
				startActivity(in);
				
			}
		});
		
		
		
		
		
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if(item.getItemId() == android.R.id.home){
			finish();
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onDestroy() {
		G.currentSearchResults = null;
		super.onDestroy();
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.activity_search_result, menu);
//		return true;
//	}

}
