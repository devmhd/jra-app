package com.devmhd.binaryblue.jra;

import org.holoeverywhere.widget.Button;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public class ProfileMainFragment extends SherlockFragment {




	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_profile_main, container, false);  

	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);


		((TextView) getView().findViewById(R.id.tvStaticAboutHeader)).setTypeface(G.robotoLight);
		((TextView) getView().findViewById(R.id.tvStaticStatusHeader)).setTypeface(G.robotoLight);




		((TextView) getView().findViewById(R.id.tvName)).setText(G.currentProfile.getName());
		((Button) getView().findViewById(R.id.btnShowOnMap)).setText(G.currentProfile.getLocation());
		((TextView) getView().findViewById(R.id.tvStatus)).setText(G.currentProfile.getStatus());
		((TextView) getView().findViewById(R.id.tvAbout)).setText(G.currentProfile.getAbout());
		((TextView) getView().findViewById(R.id.tvAge)).setText(G.currentProfile.getAge());

		((ImageView) getView().findViewById(R.id.ivApple)).setImageResource(G.getRatingImg(G.currentProfile.getAppleMatch()));

		((SquareImageView) getView().findViewById(R.id.ivProfilePic)).setImageUrl(G.currentProfile.getAvatar(), G.imageLoader);





		((Button) getView().findViewById(R.id.btnShowOnMap)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent in = new Intent(getActivity(), MapActivity.class);
				in.putExtra("multiple", false);
				in.putExtra("title", G.currentProfile.getName() + " on Map");
				startActivity(in);

			}
		});


	}



}