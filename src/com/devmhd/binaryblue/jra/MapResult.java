package com.devmhd.binaryblue.jra;

import com.google.android.gms.maps.model.LatLng;

public class MapResult {
	
	int id;
	String name;
	String avatar;
	
	LatLng latLng;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public LatLng getLatLng() {
		return latLng;
	}

	public void setLatLng(LatLng latLng) {
		this.latLng = latLng;
	}

	public MapResult(int id, String name, String avatar, LatLng latLng) {
		
		this.id = id;
		this.name = name;
		this.avatar = avatar;
		this.latLng = latLng;
	}
	
   

}
