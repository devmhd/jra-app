package com.devmhd.binaryblue.jra;

public class NavItem {

	private String name;
	private boolean hasCount;
	private int count;
	
	private int iconRes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHasCount() {
		return hasCount;
	}

	public void setHasCount(boolean hasCount) {
		this.hasCount = hasCount;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getIconRes() {
		return iconRes;
	}

	public void setIconRes(int iconRes) {
		this.iconRes = iconRes;
	}

	public NavItem(String name, boolean hasCount, int count, int iconRes) {
		super();
		this.name = name;
		this.hasCount = hasCount;
		this.count = count;
		this.iconRes = iconRes;
	}
	
}
