package com.devmhd.binaryblue.jra;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.Button;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public class FriendReqListAdapter extends ArrayAdapter<FriendRequest> {

	Context context;
	ProgressDialog progressDialog;
	ArrayList<FriendRequest> listArray;
	
	
	
	
	
	
	public FriendReqListAdapter(Context context, ArrayList<FriendRequest> listArray) {
		
		super(context, R.layout.row_friend_req, listArray);
	
		
		this.context = context;
		
		this.listArray = listArray;

		
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		
		
		
		
		View v = convertView;
		
		if(v==null)
		{
			Log.d("NULL", "VIIIEWWWW NNUUULLLLLL");
			LayoutInflater li = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			
			
			v = li.inflate(R.layout.row_friend_req, null);
		}
		
		TextView tvName;
		
		SquareImageView ivThumb;
		
		tvName = (TextView) v.findViewById(R.id.tvFriendReqName);
		tvName.setText(listArray.get(position).getName());
		tvName.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(context, ProfileActivity.class);
				context.startActivity(intent);
				
			}
		});
		
		
		ivThumb = (SquareImageView) v.findViewById(R.id.ivFriendRequest);
		ivThumb.setImageUrl(G.frequests.get(position).getAvatar(), G.imageLoader);
		
		
		ivThumb.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(context, ProfileActivity.class);
				intent.putExtra("id", listArray.get(position).getUid());
				context.startActivity(intent);
				
			}
		});
		
		
		TextView tvMessage;
		Button btnAcc, btnDeny;
		
		tvMessage = (TextView) v.findViewById(R.id.tvMessage);
		
		btnAcc = (Button) v.findViewById(R.id.btnFriendReqAccept);
		btnDeny = (Button) v.findViewById(R.id.btnFriendReqDeny);
		
		
		if(listArray.get(position).getState()==0)
		{

			btnAcc.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					int id = (Integer) v.getTag();
					
					new AcceptFriendRequestTask().execute("" + id);
					
				}
			});
			
			
			btnDeny.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					int id = (Integer) v.getTag();
					
					new DenyFriendRequestTask().execute("" + id);
					
				}
			});
			
			btnAcc.setTag(listArray.get(position).getUid());
			btnDeny.setTag(listArray.get(position).getUid());
			
			
			
		}
		
		else
		{
			btnAcc.setVisibility(View.GONE);
			btnDeny.setVisibility(View.GONE);
			tvMessage.setVisibility(View.VISIBLE);
			
		}
		
		if(listArray.get(position).getState()==1)
		{
			tvMessage.setText("Friend request accepted");
			tvMessage.setTextColor(Color.parseColor("#99CC00"));
		}
		
		if(listArray.get(position).getState()==2)
		{
			tvMessage.setText("Friend request denied");
			tvMessage.setTextColor(Color.parseColor("#FF4444"));
		}
		
		
		return v;
	}
	
	
//	private class ReqResponseListener implements OnClickListener{
//
//		private int position;
//		private boolean acc;
//		
//		public ReqResponseListener(int position, boolean acc){
//			this.position = position;
//			this.acc = acc;
//		}
//		
//		@Override
//		public void onClick(View arg0) {
//			
//			
//			Log.d("ClickPosition", ""+position);
//			
//			if(acc) G.frequests.get(position).accept();
//			else G.frequests.get(position).deny();
//			
//			notifyDataSetChanged();
//
//		}
//		
//	}
	
	
	
	
	
	
	private class AcceptFriendRequestTask extends AsyncTask<String,Void, Integer>{

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(context, "", "Accepting request...");
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(String... params) {

			@SuppressWarnings("unused")
			String profileJson = null;

			try {

				
				
				ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
				postVars.add(new BasicNameValuePair(URL.VAR_ACCEPT_FRIEND_REQ_UID, params[0]));
				
				
				profileJson = G.getString(URL.URL_BASE + URL.METHOD_ACCEPT_FRIEND_REQ, postVars, true);
			
				
				
				
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return Integer.parseInt(params[0]);
		}
		@Override
		protected void onPostExecute(Integer id) {
			progressDialog.dismiss();

			Crouton.makeText((Activity) context, "Acc", Style.CONFIRM).show();
			
			for(FriendRequest request : G.frequests){
				
				if(request.getUid() == id){
					request.accept();
					notifyDataSetChanged();
				}
			}
			
			super.onPostExecute(id);


		}

	}
	
	
	
	
	private class DenyFriendRequestTask extends AsyncTask<String,Void, Integer>{

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(context, "", "Denying request...");
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(String... params) {

			@SuppressWarnings("unused")
			String profileJson = null;

			try {

				
				
				ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
				postVars.add(new BasicNameValuePair(URL.VAR_ACCEPT_FRIEND_REQ_UID, params[0]));
				
				
				profileJson = G.getString(URL.URL_BASE + URL.METHOD_DENY_FRIEND_REQ, postVars, true);
			
				
				
				
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return Integer.parseInt(params[0]);
		}
		@Override
		protected void onPostExecute(Integer id) {
			progressDialog.dismiss();

			Crouton.makeText((Activity) context, "Deny", Style.CONFIRM).show();
			
			for(FriendRequest request : G.frequests){
				
				if(request.getUid() == id){
					request.deny();
					notifyDataSetChanged();
				}
			}
			
			super.onPostExecute(id);


		}

	}
	
	
	

}
