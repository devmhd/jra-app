package com.devmhd.binaryblue.jra;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.AlertDialog.Builder;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.EditText;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.android.volley.toolbox.NetworkImageView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;




public class ProfileActivity extends SherlockFragmentActivity implements ActionBar.TabListener{

	ProgressDialog progressDialog;
	AppSectionsPagerAdapter mAppSectionsPagerAdapter;
	ViewPager mViewPager;

	LoadProfileTask loadProfileTask;
	AddFavouriteTask addFavouriteTask;
	SendFriendRequestTask sendFriendRequestTask;
	SendMessageTask sendMessageTask;
	
	
	
	

	int id;
	boolean isFriend = true;

	NetworkImageView niv;

	private Builder addFriendDialogBuilder, sendMessageDialogBuilder;
	private AlertDialog addFriendDiag, sendMessageDiag;

	private String[] CONTENT = {
			"Profile",
			"Info",
			"LOOKING FOR"
	};


	boolean isFav = false;
	
	@Override
	protected void onResume() {
		
		setTitle("");
		loadProfileTask = new LoadProfileTask();
		addFavouriteTask = new AddFavouriteTask();
		sendFriendRequestTask = new SendFriendRequestTask();
		sendMessageTask = new SendMessageTask();
		
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		loadProfileTask.cancel(true);
		addFavouriteTask.cancel(true);
		sendFriendRequestTask.cancel(true);
		sendMessageTask.cancel(true);
		super.onPause();
	}


	private void initDiag(){
		addFriendDialogBuilder = new AlertDialog.Builder(this);		
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		final View diagView = inflater.inflate(R.layout.dialog_friend_req, null);
		addFriendDialogBuilder.setView(diagView);
		addFriendDialogBuilder.setTitle("Add " + G.currentProfile.getName());
		((TextView)diagView.findViewById(R.id.tvAddFriend)).setText("Send Contact add request to " + G.currentProfile.getName() +"?");
		addFriendDialogBuilder.setPositiveButton("Send Request", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {

				sendFriendRequestTask.execute();

			}
		});

		addFriendDialogBuilder.setNegativeButton("Cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				addFriendDiag.cancel();

			}
		});

		addFriendDiag = addFriendDialogBuilder.create();

		//=============================

		sendMessageDialogBuilder = new AlertDialog.Builder(this);		

		final View diagMsgView = inflater.inflate(R.layout.dialog_send_message, null);
		sendMessageDialogBuilder.setView(diagMsgView);
		((EditText) diagMsgView.findViewById(R.id.etMessage)).setText("");
		sendMessageDialogBuilder.setTitle("New Message");
		sendMessageDialogBuilder.setPositiveButton("Send", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {

				String msg = ((EditText) diagMsgView.findViewById(R.id.etMessage)).getText().toString();
				sendMessageTask.execute(msg);

			}
		});

		sendMessageDialogBuilder.setNegativeButton("Cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				sendMessageDiag.cancel();

			}
		});

		sendMessageDiag = sendMessageDialogBuilder.create();

	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		// Show the Up button in the action bar.
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);


		id = getIntent().getExtras().getInt("id");

		
		niv = (NetworkImageView) findViewById(R.id.nivMyProfilePic);
		loadProfileTask = new LoadProfileTask();
		loadProfileTask.execute();




	}


	void initPager(){
		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());


		//ActionBar gets initiated
		final ActionBar actionbar = getSupportActionBar();

		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);


		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mAppSectionsPagerAdapter);

		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionbar.setSelectedNavigationItem(position);
			}
		});

		for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
			actionbar.addTab(
					actionbar.newTab()
					.setText(mAppSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getSupportMenuInflater().inflate(R.menu.activity_profilet, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			finish();
			return true;

		case R.id.menu_fav:

			if(isFav){
				Crouton.makeText(this, G.currentProfile.getName() + " is already a favourite", Style.INFO).show();
			}
			else{

				addFavouriteTask.execute();
			}

			return true;

		case R.id.menu_add_friend:
			addFriendDiag.show();
			return true;

		case R.id.menu_send_message:
			sendMessageDiag.show();
			return true;



		}
		return super.onOptionsItemSelected(item);
	}


	private class AppSectionsPagerAdapter extends FragmentPagerAdapter {

		public AppSectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {


			Fragment f = null;

			switch (i) {
			case 0:

				f = new ProfileMainFragment();
				break;

			case 1:
				f = new ProfileInfoFragment();
				break;

			case 2:
				f = new ProfileLookinFragment();
				break;


			}

			return f;

		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return CONTENT[position];
		}
	}


	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem switchButton = menu.findItem(R.id.menu_fav);
		MenuItem addFriendButton = menu.findItem(R.id.menu_add_friend);
		if(isFav){
			switchButton.setIcon(R.drawable.ic_action_favourite);
			switchButton.setTitle("Favourite");
		}else{
			switchButton.setIcon(R.drawable.ic_action_favourite_holo);
			switchButton.setTitle("Add to favourites");
		}

		if(isFriend) addFriendButton.setVisible(false);

		return super.onPrepareOptionsMenu(menu);

	}






	@Override
	protected void onDestroy() {
		Crouton.cancelAllCroutons();




		super.onDestroy();
	}


	private class LoadProfileTask extends AsyncTask<Void,Void, JSONObject>{

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(ProfileActivity.this, "", "Loading");
			super.onPreExecute();
		}

		@Override
		protected JSONObject doInBackground(Void... params) {

			JSONObject profileJson = null;

			try {

				ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
				postVars.add(new BasicNameValuePair(URL.VAR_GET_PROFILE, ""+id));

				profileJson = G.getJsonObject(URL.URL_BASE + URL.METHOD_GET_PROFILE, postVars, true);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return profileJson;
		}
		@Override
		protected void onPostExecute(JSONObject result) {
			progressDialog.dismiss();
			Log.d("RESPONSE", result.toString());

			G.currentProfile = new Profile(result);


			G.currentProfile.setId(id);
			initPager();
			setTitle(G.currentProfile.getName());

			isFav = G.currentProfile.isFav();
			isFriend = G.currentProfile.isFriend();
			invalidateOptionsMenu();

			initDiag();
			super.onPostExecute(result);


		}

	}




	private class SendFriendRequestTask extends AsyncTask<Void,Void, String>{

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(ProfileActivity.this, "", "Adding Contact...");
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {

			String profileJson = null;

			try {



				profileJson = G.getString(URL.URL_BASE + URL.METHOD_ADD_CONTACT +"?" + URL.VAR_ADD_CONTACT_ID + "=" + id, null, false);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return profileJson;
		}
		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();

			Crouton.makeText(ProfileActivity.this, G.currentProfile.getName() + result, Style.CONFIRM).show();

			super.onPostExecute(result);


		}

	}





	private class AddFavouriteTask extends AsyncTask<Void,Void, String>{

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(ProfileActivity.this, "", "Adding as Favourite...");
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {

			String profileJson = null;

			try {



				profileJson = G.getString(URL.URL_BASE + URL.METHOD_ADD_FAVOURITE +"?" + URL.VAR_ADD_FAVOURITE + "=" + id, null, false);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return profileJson;
		}
		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();

			Crouton.makeText(ProfileActivity.this, G.currentProfile.getName() + " has been "+ result, Style.CONFIRM).show();
			isFav = true;
			invalidateOptionsMenu();

			super.onPostExecute(result);


		}

	}


	private class SendMessageTask extends AsyncTask<String,Void, String>{

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(ProfileActivity.this, "", "Sending Message...");
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {

			String profileJson = null;

			try {

				ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
				postVars.add(new BasicNameValuePair(URL.VAR_SEND_MESSAGE_BODY, params[0]));
				postVars.add(new BasicNameValuePair(URL.VAR_SEND_MESSAGE_ID, ""+id));

				profileJson = G.getString(URL.URL_BASE + URL.METHOD_SEND_MESSAGE, postVars, true);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return profileJson;
		}
		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();

			Crouton.makeText(ProfileActivity.this, result, Style.CONFIRM).show();
			isFav = true;
			invalidateOptionsMenu();

			super.onPostExecute(result);


		}

	}





}
