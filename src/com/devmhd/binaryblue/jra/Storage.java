package com.devmhd.binaryblue.jra;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Storage {
	
	public static boolean isLoggedIn(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean("isLoggedIn", false);


	}
	
	
	public static boolean isAgreedToTos(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean("isAgreedToTos", false);


	}
	
	public static boolean setAgreedToTos(Context ctx, boolean isAgreedToTos)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putBoolean("isAgreedToTos", isAgreedToTos);

		return editor.commit();
	}
	
	
	public static boolean isSearchResultsLocallyStored(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean("isSearchResultsLocallyStored", false);


	}
	
	public static boolean setSearchResultsLocallyStored(Context ctx, boolean isSearchResultsLocallyStored)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putBoolean("isSearchResultsLocallyStored", isSearchResultsLocallyStored);

		return editor.commit();
	}
	
	public static boolean isFavouritesLocallyStored(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean("isFavouritesLocallyStored", false);


	}
	
	public static boolean setFavouritesLocallyStored(Context ctx, boolean input)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putBoolean("isFavouritesLocallyStored", input);

		return editor.commit();
	}
	
	public static boolean isFriendsLocallyStored(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean("isFriendsLocallyStored", false);


	}
	
	public static boolean setFriendsLocallyStored(Context ctx, boolean input)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putBoolean("isFriendsLocallyStored", input);

		return editor.commit();
	}
	
	
	public static boolean isFriendRequestsLocallyStored(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean("isFriendRequestsLocallyStored", false);


	}
	
	public static boolean setFriendRequestsLocallyStored(Context ctx, boolean input)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putBoolean("isFriendRequestsLocallyStored", input);

		return editor.commit();
	}
	
	public static boolean setLoggedIn(Context ctx, boolean loggedIn)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putBoolean("isLoggedIn", loggedIn);

		return editor.commit();
	}
	
	public static String getLoggerEmail(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getString("loggerEmail", "abc@example.com");


	}
	
	public static boolean setLoggerEmail(Context ctx, String email)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putString("loggerEmail", email);

		return editor.commit();


	}
	
	public static String getLoggerPassword(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getString("loggerPassword", "abc@example.com");


	}
	
	public static boolean setLoggerPassword(Context ctx, String pass)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putString("loggerPassword", pass);

		return editor.commit();


	}
	
	public static int getKeepMeAliveValue(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getInt("keepmealive", 0);


	}
	
	public static boolean setKeepMeAliveValue(Context ctx, int value)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putInt("keepmealive", value);

		return editor.commit();


	}
	
	
	public static int getRemindMeValue(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getInt("remindmeafter", 0);


	}
	
	public static boolean setRemindMeValue(Context ctx, int value)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putInt("remindmeafter", value);

		return editor.commit();


	}
	
	
	public static boolean isChatOnStored(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean("chaton", false);


	}
	
	public static boolean setChatOn(Context ctx, boolean input)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putBoolean("chaton", input);

		return editor.commit();
	}
	
	
	public static boolean isNotificationOn(Context ctx)
	{

		return PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean("isNotificationOn", false);


	}
	
	public static boolean setNotificationOn(Context ctx, boolean input)
	{

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.putBoolean("isNotificationOn", input);

		return editor.commit();
	}
	
	
	
	public static boolean clearAll(Context ctx){
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
		editor.remove("isLoggedIn");
		editor.remove("loggerEmail");
		editor.remove("loggerPassword");
	
		return editor.commit();
	}
	
	
	

}
