package com.devmhd.binaryblue.jra;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;

public class SquareImageView extends NetworkImageView {
	
	private static final int FADE_IN_TIME_MS = 250;
	
    public SquareImageView(Context context) {
        super(context);
        
        setDefaultImageResId(R.drawable.loading);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        setDefaultImageResId(R.drawable.loading);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
  
        setDefaultImageResId(R.drawable.loading);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
    }
    
    @Override
    public void setImageBitmap(Bitmap bm) {
        TransitionDrawable td = new TransitionDrawable(new Drawable[]{
                new ColorDrawable(android.R.color.transparent),
                new BitmapDrawable(getContext().getResources(), bm)
        });
 
        setImageDrawable(td);
        td.startTransition(FADE_IN_TIME_MS);
    }
}