package com.devmhd.binaryblue.jra;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;

@SuppressLint("NewApi")
public class G {
	
	
	public static final String DEBUG_TAG = "JRAAPP";
	
	public static ArrayList<ProfileInfo> staticProfileInfos;
	
	static Context cotx;
	static HttpClient httpclient;
	
	public static Me me;
	public static Profile currentProfile;
	public static ArrayList<SearchResult> currentSearchResults;
	
	public static ArrayList<SearchResult> contacts;
	public static ArrayList<SearchResult> favourites;
	
	public static ArrayList<Contact> contactsToInvite;
	
	
	
	public static ArrayList<SearchResult> allResults;
	
	public static ArrayList<MapResult> currentMapResults;
	
	public static int myId;
	
	public static LatLng initialPosition = null;
	

	public static ArrayList<NavItem> navItems;
	public static ArrayList<FriendRequest> frequests;
	public static ArrayList<Conversation> inbox;
	
	public static ImageLoader imageLoader;
	
	public static RequestQueue reqQueue;
	
	public static final String[] starNames = {
		"Amy",
		"Rajesh",
		"Sheldon",
		"Berny",
		"Penny",
		"Leonard",
		"Wolowitz"
	};
	
	
	
	
public static Typeface robotoLight;
	
	public static final void init(Context ctx){
		robotoLight = Typeface.createFromAsset(ctx.getAssets(), "robotolight.ttf");
		
		httpclient = getThreadSafeClient();
		
		cotx = ctx;
		reqQueue = Volley.newRequestQueue(ctx);
		imageLoader = new ImageLoader(reqQueue, new DiskBitmapCache(ctx.getCacheDir()));
		
//		persons = new ArrayList<Person>();
//		persons.add(new Person("Amy", R.drawable.person1, new LatLng(-33.77305097143649, 148.60431731562497)));
//		persons.add(new Person("Rajesh", R.drawable.person2, new LatLng(-33.33128479042631, 148.00006926874997)));
//		persons.add(new Person("Sheldon", R.drawable.person3, new LatLng(-33.59020962083333, 147.56336272578122)));
//		persons.add(new Person("Berny", R.drawable.person4, new LatLng(-33.73422972964348, 150.76313079218747)));
//		persons.add(new Person("Penny", R.drawable.person5, new LatLng(-34.230719449323296, 150.58734954218747)));
//		persons.add(new Person("Leonard", R.drawable.person6, new LatLng(-33.885793177873445, 151.22249663691403)));
//		persons.add(new Person("Wolowitz", R.drawable.person7, new LatLng(-33.89377307270687, 151.1988073668945)));
//		
//		
		navItems = new ArrayList<NavItem>();
		
		navItems.add(new NavItem("Home", false, 0, R.drawable.ic_nav_home));
		navItems.add(new NavItem("My Profile", false, 0, R.drawable.ic_nav_home));
		navItems.add(new NavItem("Messages", false, 3, R.drawable.ic_nav_inbox));
		navItems.add(new NavItem("Contact Requests", false, 3, R.drawable.ic_nav_friend_req));
		navItems.add(new NavItem("Favourites", false, 12, R.drawable.ic_nav_friends));
		navItems.add(new NavItem("Contacts", false, 12, R.drawable.ic_nav_friends));
		navItems.add(new NavItem("Favourites MAP", false, 0, R.drawable.ic_nav_fav_location));
		navItems.add(new NavItem("Contacts MAP", false, 0, R.drawable.ic_nav_live_map));
		navItems.add(new NavItem("JRA LIVE", false, 0, R.drawable.ic_nav_live_map));
		
		
		navItems.add(new NavItem("invite Contacts", false, 0, R.drawable.ic_invite_contacts));
		navItems.add(new NavItem("Settings", false, 0, R.drawable.ic_settings));
		navItems.add(new NavItem("Log out", false, 0, R.drawable.ic_logout));
		
		
		
//		friends = new ArrayList<Person>();
//		friends.add(new Person("Penny", R.drawable.person5, new LatLng(-34.230719449323296, 150.58734954218747)));
//		friends.add(new Person("Amy", R.drawable.person1, new LatLng(-33.77305097143649, 148.60431731562497)));
//		friends.add(new Person("Leonard", R.drawable.person6, new LatLng(-33.885793177873445, 151.22249663691403)));
//		friends.add(new Person("Sheldon", R.drawable.person3, new LatLng(-33.59020962083333, 147.56336272578122)));
//		friends.add(new Person("Rajesh", R.drawable.person2, new LatLng(-33.33128479042631, 148.00006926874997)));
//		friends.add(new Person("Berny", R.drawable.person4, new LatLng(-33.73422972964348, 150.76313079218747)));
//		friends.add(new Person("Wolowitz", R.drawable.person7, new LatLng(-33.89377307270687, 151.1988073668945)));
//		
//		
		
		
		
//		sresults = new ArrayList<SearchResult>();
//		sresults.add(new SearchResult(4, "Fun Loving girl. Love to go out"));
//		sresults.add(new SearchResult(1, "Looking for that special someone"));
//		sresults.add(new SearchResult(3, "I'm 18 and I'm dangerous."));
//		sresults.add(new SearchResult(2, "An honest, responsible, genuine and fun loving person who has a great sense of humor."));
//		sresults.add(new SearchResult(0, "I'm a single mum of two boys and i also study as well."));
//		
		
	}

	
	public static boolean isNetworkConnected(Context ctx) {
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		return (cm.getActiveNetworkInfo() != null);
	}
	
	public static JSONObject getJsonObject(String URL, List<NameValuePair> posts, boolean isPost) throws UnsupportedEncodingException
	{


		
		HttpPost httpPost = null;
		HttpGet httpGet = null;

		if(isPost) 
		{
			httpPost = new HttpPost(URL);
			httpPost.setEntity(new UrlEncodedFormEntity(posts));


		}
		else httpGet = new HttpGet(URL);

		
		JSONObject jOb = null;

		try {
			HttpResponse response = httpclient.execute(isPost?httpPost:httpGet);

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			for (String line = null; (line = reader.readLine()) != null;) {
				builder.append(line).append("\n");
			}


			jOb = new JSONObject(builder.toString());
		}
		catch(Exception e) {
			Log.d(URL, e.toString());
		}



		return jOb;

	}
	
	
	
	
	
	public static JSONArray getJsonArray(String URL, List<NameValuePair> posts, boolean isPost) throws UnsupportedEncodingException
	{


		
		HttpPost httpPost = null;
		HttpGet httpGet = null;

		if(isPost) 
		{
			httpPost = new HttpPost(URL);
			httpPost.setEntity(new UrlEncodedFormEntity(posts));


		}
		else httpGet = new HttpGet(URL);

		
		JSONArray jOb = null;

		try {
			HttpResponse response = httpclient.execute(isPost?httpPost:httpGet);

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			for (String line = null; (line = reader.readLine()) != null;) {
				builder.append(line).append("\n");
			}


			jOb = new JSONArray(builder.toString());
		}
		catch(Exception e) {
			Log.d(URL, e.toString());
		}



		return jOb;

	}
	
	
	
	
	public static String getString(String URL, List<NameValuePair> posts, boolean isPost) throws UnsupportedEncodingException
	{


		
		HttpPost httpPost = null;
		HttpGet httpGet = null;

		if(isPost) 
		{
			httpPost = new HttpPost(URL);
			httpPost.setEntity(new UrlEncodedFormEntity(posts));


		}
		else httpGet = new HttpGet(URL);

		
		String jOb = null;

		try {
			HttpResponse response = httpclient.execute(isPost?httpPost:httpGet);

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			for (String line = null; (line = reader.readLine()) != null;) {
				builder.append(line).append("\n");
			}


			jOb = builder.toString();
		}
		catch(Exception e) {
			Log.d(URL, e.toString());
		}



		return jOb;

	}
	
	
	
	public static JSONObject getJsonObjectWithJson(String URL, JSONArray jArray, boolean isPost) throws UnsupportedEncodingException
	{


		
		HttpPost httpPost = null;
		HttpGet httpGet = null;

		if(isPost) 
		{
			httpPost = new HttpPost(URL);
			
			StringEntity params =new StringEntity(jArray.toString());
			httpPost.addHeader("Content-type","text/json; charset=utf-8");
			httpPost.setEntity(params);
			
			


		}
		else httpGet = new HttpGet(URL);

		
		JSONObject jOb = null;

		try {
			HttpResponse response = httpclient.execute(isPost?httpPost:httpGet);

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			for (String line = null; (line = reader.readLine()) != null;) {
				builder.append(line).append("\n");
			}


			jOb = new JSONObject(builder.toString());
		}
		catch(Exception e) {
			Log.d(URL, e.toString());
		}



		return jOb;

	}
	
	
	
	
	public static int getRatingImg(int rating){
		
		if(rating==0) return R.drawable.rating0;
		if(rating==1) return R.drawable.rating1;
		if(rating==2) return R.drawable.rating2;
		if(rating==3) return R.drawable.rating3;
		if(rating==4) return R.drawable.rating4;
		return R.drawable.rating5;
	}
	
	
	public static int dpToPx(int dps){
		
		final float scale = cotx.getResources().getDisplayMetrics().density;
		return (int) (dps * scale + 0.5f);
	}
	
	public static DefaultHttpClient getThreadSafeClient()  {

	    DefaultHttpClient client = new DefaultHttpClient();
	    ClientConnectionManager mgr = client.getConnectionManager();
	    HttpParams params = client.getParams();
	    client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, 

	            mgr.getSchemeRegistry()), params);
	    return client;
	}
	
	
	@SuppressLint("NewApi")
	public static void executeParallelly(AsyncTask<Object, Object, Object> task, Object params){
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			   task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
			} else {
			   task.execute(params);
			}
	}
	
	
	
	
	
	
}
