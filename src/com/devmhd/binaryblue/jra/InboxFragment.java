package com.devmhd.binaryblue.jra;


import java.util.ArrayList;

import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.AdapterView.OnItemSelectedListener;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.Spinner;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockFragment;

public class InboxFragment extends SherlockFragment {


	private ProgressDialog pd;

	LoadInboxTask loadInboxTask;
	ImageButton ibtnRefresh;
	ListView lv;
	Spinner spMessage;
	LinearLayout llNoResult;

	String[] messageOptions = {
			"All Messages",
			"Inbox",
			"Sent Messages"
	};


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_inbox, container, false);  

	}

@Override
public void onActivityCreated(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onActivityCreated(savedInstanceState);


		
		
		
		lv = (ListView) getView().findViewById(R.id.lvInbox);
		llNoResult = (LinearLayout) getView().findViewById(R.id.llNoResult);
		spMessage = (Spinner) getView().findViewById(R.id.spMessages);

		ArrayAdapter<String> spAdapter = new ArrayAdapter<String>(getActivity(), org.holoeverywhere.R.layout.simple_spinner_item, messageOptions);
		spAdapter.setDropDownViewResource(org.holoeverywhere.R.layout.simple_spinner_dropdown_item);
		spMessage.setAdapter(spAdapter);

		loadInboxTask = new LoadInboxTask();
		loadInboxTask.execute();

		ibtnRefresh = (ImageButton) getView().findViewById(R.id.ibtnRefresh);
		ibtnRefresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				loadInboxTask = new LoadInboxTask();
				loadInboxTask.execute();

			}
		});



	}





	private class LoadInboxTask extends AsyncTask<Void, Void, Void>{


		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(getActivity(), "", "Loading Messages...");
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			JSONArray inboxArray = null;

			try {

				inboxArray = G.getJsonArray(URL.URL_BASE + URL.METHOD_GET_INBOX, null, false);

				G.inbox = new ArrayList<Conversation>();





				for(int i=0; i<inboxArray.length(); ++i){

					JSONObject msgJson = inboxArray.getJSONObject(i);


					G.inbox.add(new Conversation(

							msgJson.getString("msg"),

							Integer.parseInt(msgJson.getString("from")), 
							msgJson.getString("uname_from"),
							msgJson.getString("avatar_from"),

							Integer.parseInt(msgJson.getString("to")),

							msgJson.getString("uname_to"),
							msgJson.getString("avatar_to")


							));


				}









			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Log.d("INBOX_RESPONSE", inboxArray.toString());
			return null;


		}

		protected void onPostExecute(Void resultArray) {




			pd.dismiss();


			ArrayList<Conversation> result = getFilteredMessages(0);

			if(result.isEmpty()) llNoResult.setVisibility(View.VISIBLE);
			else lv.setVisibility(View.VISIBLE);

			lv.setAdapter(new InboxConversationsAdapter(getActivity(), result));


			spMessage.setOnItemSelectedListener(new OnItemSelectedListener() {



				@Override
				public void onItemSelected(
						org.holoeverywhere.widget.AdapterView<?> arg0, View arg1,
						int pos, long arg3) {

					ArrayList<Conversation> result = getFilteredMessages(pos);

					if(result.isEmpty()){ 
						llNoResult.setVisibility(View.VISIBLE);
						lv.setVisibility(View.GONE);
					}
					else{
						lv.setVisibility(View.VISIBLE);
						llNoResult.setVisibility(View.GONE);
					}

				}

				@Override
				public void onNothingSelected(
						org.holoeverywhere.widget.AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});


		};

	}


	private ArrayList<Conversation> getFilteredMessages(int selection) {

		if(selection == 0) return G.inbox;

		if(selection == 2){

			ArrayList<Conversation> convs = new ArrayList<Conversation>();
			for(Conversation conv : G.inbox){
				if(conv.getSenderId() == G.myId)
					convs.add(conv);
			}

			return convs;

		}
		else {
			ArrayList<Conversation> convs = new ArrayList<Conversation>();
			for(Conversation conv : G.inbox){
				if(conv.getReceiverId() == G.myId)
					convs.add(conv);
			}

			return convs;
		}

	}




	@Override
	public void onDestroy() {
		if(loadInboxTask != null) loadInboxTask.cancel(true);
		super.onDestroy();
	}

}